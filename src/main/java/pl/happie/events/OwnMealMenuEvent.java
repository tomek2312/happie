package pl.happie.events;

/**
 * Event changes possibility of click button in menu.
 *
 *@author Tomasz Trybała
 */
public class OwnMealMenuEvent {
    private String mValue;

    public OwnMealMenuEvent(){
        mValue = null;
    }

    public OwnMealMenuEvent(String value){
        mValue = value;
    }

    public String getValue(){
        return mValue;
    }

    public boolean isValue(){
        return mValue != null;
    }
}
