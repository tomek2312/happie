package pl.happie.listeners;

/**
 *@author Tomasz Trybała
 */
public interface ChangeMealListener {
    void getMealName(String mealName);
}
