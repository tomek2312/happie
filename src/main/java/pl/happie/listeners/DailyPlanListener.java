package pl.happie.listeners;

/**
 *@author Tomasz Trybała
 */
public interface DailyPlanListener {
    void onSuccess();
    void onChangeImpossible();
    void onPlanAdapted();
}
