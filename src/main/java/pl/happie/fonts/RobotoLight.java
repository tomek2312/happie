package pl.happie.fonts;

import android.graphics.Typeface;

import pl.happie.HappieApplication;

/**
 * Singleton class.
 * Returns Roboto Light Typeface.
 *
 *@author Tomasz Trybała
 */
public class RobotoLight {
    private static Typeface sTypeface;

    public static Typeface getTypeface(){
        if(sTypeface == null){
            sTypeface = Typeface.createFromAsset(HappieApplication.getAppContext().getAssets(),
                    "fonts/RobotoLight.ttf");
        }

        return sTypeface;
    }
}
