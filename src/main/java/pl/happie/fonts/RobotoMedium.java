package pl.happie.fonts;

import android.graphics.Typeface;

import pl.happie.HappieApplication;

/**
 * Singleton class.
 * Returns Roboto Medium Typeface.
 *
 *@author Tomasz Trybała
 */
public class RobotoMedium {
    private static Typeface sTypeface;

    public static Typeface getTypeface(){
        if(sTypeface == null){
            sTypeface = Typeface.createFromAsset(HappieApplication.getAppContext().getAssets(),
                    "fonts/RobotoMedium.ttf");
        }

        return sTypeface;
    }
}
