package pl.happie.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import pl.happie.ConstantsAPI;
import pl.happie.HappieApplication;
import pl.happie.R;
import pl.happie.activities.RegisterActivity;
import pl.happie.fonts.RobotoLight;
import pl.happie.net.requests.RegisterInfoRequest;
import roboguice.inject.InjectView;

/**
 * Fragment allows user to set gender,
 * weight, height and age
 *
 * @author Tomasz Trybała
 */
public class RegisterInfoFragment extends RoboEventFragment {
    //region constants
    private static final int MAX_AGE = 82;
    private static final int MIN_AGE = 18;
    private static final int DEFAULT_AGE = 30;

    private static final int MIN_WEIGHT = 30; //kg
    private static final int MAX_WEIGHT = 170; //kg
    private static final int DEFAULT_WEIGHT = 65; //kg
    private static final double KG_TO_IBS = 2.2;

    private static final int MIN_HEIGHT = 61;
    private static final int MAX_HEIGHT = 183;
    private static final int DEFAULT_HEIGHT = 170; //cm
    private static final int MIN_HEIGHT_IMPERIAL = 48;
    private static final int MAX_HEIGHT_IMPERIAL = 144;
    //endregion

    //region views
    @InjectView(R.id.txtvRegisterInfoTitle)
    private TextView mTxtvTitle;

    @InjectView(R.id.txtvRegisterInfoAge)
    private TextView mTxtvAgeValue;

    @InjectView(R.id.txtvRegisterInfoWeight)
    private TextView mTxtvWeightValue;

    @InjectView(R.id.txtvRegisterInfoHeight)
    private TextView mTxtvHeightValue;

    @InjectView(R.id.txtvRegisterInfoLb)
    private TextView mTxtvWeightImperial;

    @InjectView(R.id.txtvRegisterInfoKg)
    private TextView mTxtvWeightMetric;

    @InjectView(R.id.txtvRegisterInfoFt)
    private TextView mTxtvHeightImperial;

    @InjectView(R.id.txtvRegisterInfoCm)
    private TextView mTxtvHeightMetric;

    @InjectView(R.id.llRegisterInfoFemale)
    private LinearLayout mLlFemale;

    @InjectView(R.id.llRegisterInfoMale)
    private LinearLayout mLlMale;

    @InjectView(R.id.imgvRegisterAgeMinus)
    private ImageView mImgvAgeMinus;

    @InjectView(R.id.imgvRegisterWeightMinus)
    private ImageView mImgvWeightMinus;

    @InjectView(R.id.imgvRegisterHeightMinus)
    private ImageView mImgvHeightMinus;

    @InjectView(R.id.imgvRegisterAgePlus)
    private ImageView mImgvAgePlus;

    @InjectView(R.id.imgvRegisterWeightPlus)
    private ImageView mImgvWeightPlus;

    @InjectView(R.id.imgvRegisterHeightPlus)
    private ImageView mImgvHeightPlus;

    @InjectView(R.id.sbRegisterInfoAge)
    private SeekBar mSbAge;

    @InjectView(R.id.sbRegisterInfoWeight)
    private SeekBar mSbWeight;

    @InjectView(R.id.sbRegisterInfoHeight)
    private SeekBar mSbHeight;

    @InjectView(R.id.llRegisterInfoNext)
    private LinearLayout mLlNext;
    //endregion

    //region variables
    private RegisterActivity mActivity;
    private boolean mIsMale;
    private boolean mIsMetric;
    private int mAgeValue;
    private int mWeightValue;
    private int mHeightValue;
    //endregion

    /**
     * Method sets fonts.
     */
    private void setFonts() {
        mTxtvTitle.setTypeface(RobotoLight.getTypeface());
    }

    /**
     * Method sets text to title view from html.
     */
    private void setText() {
        mTxtvTitle.setText(Html.fromHtml(getResources().getString(R.string.registration_text1)));
    }

    /**
     * Method sets correct toolbar title.
     */
    private void setToolbarTitle() {
        mActivity.setActionBarTitle(getResources().getString(R.string.register_step_1_title));
    }

    /**
     * Method sets listeners to change sex.
     */
    private void setSexListeners() {
        mLlFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLlMale.setSelected(false);
                mLlFemale.setSelected(true);
                mIsMale = false;
            }
        });

        mLlMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLlFemale.setSelected(false);
                mLlMale.setSelected(true);
                mIsMale = true;
            }
        });
    }

    /**
     * Method sets listener to seek bar of age,
     * sets also max value and default value.
     */
    private void setAgeSeekBar() {
        mSbAge.setMax(MAX_AGE);
        mSbAge.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAgeValue = progress + MIN_AGE;
                mTxtvAgeValue.setText(mAgeValue + " " + getString(R.string.years));
            }
        });

        mSbAge.setProgress(DEFAULT_AGE - MIN_AGE);
    }

    /**
     * Method sets listeners
     * to image views of minus and plus.
     */
    private void setSeekBarsButtons() {
        mImgvAgeMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSbAge.setProgress(mSbAge.getProgress() - 1);
            }
        });

        mImgvHeightMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSbHeight.getProgress() > 0) {
                    mHeightValue--;
                    mSbHeight.setProgress(mSbHeight.getProgress() - 1);
                }
            }
        });

        mImgvWeightMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSbWeight.getProgress() > 0) {
                    mWeightValue--;
                    mSbWeight.setProgress(mSbWeight.getProgress() - 1);
                }
            }
        });

        mImgvAgePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSbAge.setProgress(mSbAge.getProgress() + 1);
            }
        });

        mImgvHeightPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSbHeight.getProgress() < mSbHeight.getMax()) {
                    mHeightValue++;
                    mSbHeight.setProgress(mSbHeight.getProgress() + 1);
                }
            }
        });

        mImgvWeightPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSbWeight.getProgress() < mSbWeight.getMax()) {
                    mWeightValue++;
                    mSbWeight.setProgress(mSbWeight.getProgress() + 1);
                }
            }
        });
    }

    /**
     * Method sets listeners to views that
     * change kind of units between
     * metric and imperial.
     */
    private void setUnitsListeners() {
        mTxtvHeightImperial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mIsMetric) {
                    setUnits(false);
                }
            }
        });

        mTxtvHeightMetric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mIsMetric) {
                    setUnits(true);
                }
            }
        });

        mTxtvWeightImperial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mIsMetric) {
                    setUnits(false);
                }
            }
        });

        mTxtvWeightMetric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mIsMetric) {
                    setUnits(true);
                }
            }
        });
    }

    /**
     * Method set listener to view that finishes
     * form.
     */
    private void setNextListener() {
        mLlNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendDataOnServer();
            }
        });
    }

    /**
     * Method sends data on server.
     */
    private void sendDataOnServer() {
        mActivity.showDialog();

        JSONObject object = RegisterInfoRequest.getJson(mActivity,
                mAgeValue, mIsMale, mIsMetric, mHeightValue, mWeightValue);
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.PUT,
                ConstantsAPI.REGISTRATION_STEP_1, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject jsonObject = response.getJSONObject(RegisterActivity.TEMPORARY_DATA);
                    mActivity.setTemporaryToken(jsonObject.getString(RegisterActivity.TEMPORARY_TOKEN));
                    mActivity.setGender(mIsMale);
                    mActivity.setMetric(mIsMetric);
                    mActivity.setDefaultWeight(mWeightValue);
                    mActivity.setFragment(new RegisterFigureFragment());
                } catch (JSONException e) {
                    mActivity.showToast(getResources().getString(R.string.register_data_error_default));
                } finally {
                    mActivity.hideDialog();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mActivity.hideDialog();
                        if (error instanceof NoConnectionError) {
                            mActivity.showToast(getResources().getString(R.string.register_data_internet_failure));
                        } else {
                            mActivity.showToast(getResources().getString(R.string.register_data_error_default));
                        }
                    }
                });

        HappieApplication.getInstance().getRequestQueue().add(objectRequest);
    }

    /**
     * Method return current value in percent.
     *
     * @param min     min value of range
     * @param max     max value of range
     * @param current current value of range
     * @return percent current value
     */
    private int getPercentProgress(int min, int max, int current) {
        float numerator = (float) (current - min);
        float denominator = (float) (max - min);
        float fraction = numerator / denominator;
        return (int) (fraction * denominator);
    }

    /**
     * Set views that depend of kind of units.
     *
     * @param isMetric if currently units are metric or imperial
     */
    private void setUnits(boolean isMetric) {
        mTxtvHeightImperial.setSelected(!isMetric);
        mTxtvWeightImperial.setSelected(!isMetric);
        mTxtvHeightMetric.setSelected(isMetric);
        mTxtvWeightMetric.setSelected(isMetric);
        mIsMetric = isMetric;
        changeValues();
    }

    /**
     * Method sets listener to weight seek bar.
     * Sets also max and default value.
     */
    private void setWeightSeekBar() {
        mSbWeight.setMax(MAX_WEIGHT);
        mSbWeight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mWeightValue = progress + (mIsMetric ?
                            MIN_WEIGHT :
                            (int) (MIN_WEIGHT * KG_TO_IBS));
                }

                mTxtvWeightValue.setText(mWeightValue + " " +
                        getResources().getString(mIsMetric ? R.string.kg : R.string.lb));
            }
        });

        mWeightValue = DEFAULT_WEIGHT;
        mSbWeight.setProgress(getPercentProgress(
                MIN_WEIGHT,
                MAX_WEIGHT,
                DEFAULT_WEIGHT));
    }

    /**
     * Method sets listener to height seek bar.
     * Sets also max and default value.
     */
    private void setHeightSeekBar() {
        mSbHeight.setMax(MAX_HEIGHT);
        mSbHeight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mHeightValue = progress + (mIsMetric ? MIN_HEIGHT : MIN_HEIGHT_IMPERIAL);
                }

                setHeightTextValue();
            }
        });

        mHeightValue = DEFAULT_HEIGHT;
        mSbHeight.setProgress(getPercentProgress(
                MIN_HEIGHT,
                MAX_HEIGHT,
                DEFAULT_HEIGHT));
    }

    /**
     * Method sets correctly value of current height.
     */
    private void setHeightTextValue() {
        String value;
        if (mIsMetric) {
            value = mHeightValue + " " + getString(R.string.cm);
        } else {

            value = (mHeightValue / 24)
                    + " "
                    + getString(R.string.ft)
                    + (mHeightValue % 24 == 0 ? "" : " "
                    + (((mHeightValue % 24 % 2) == 0) ? "" + ((mHeightValue % 24) / 2)
                    : ((mHeightValue % 24) / 2) + ",5") + " "
                    + getString(R.string.inch));
        }

        mTxtvHeightValue.setText(value);
    }

    /**
     * Method change values in seek bars
     * according to current unit.
     */
    private void changeValues() {
        int minWeight;
        int minHeight;

        if (mIsMetric) {
            minWeight = MIN_WEIGHT;
            mWeightValue = (int) (mWeightValue / KG_TO_IBS);
            if (mWeightValue < MIN_WEIGHT) {
                mWeightValue = MIN_WEIGHT;
            }

            mSbWeight.setMax(MAX_WEIGHT);

            minHeight = MIN_HEIGHT;
            mHeightValue = (int) (((float) ((mHeightValue - MIN_HEIGHT_IMPERIAL) * (MAX_HEIGHT - MIN_HEIGHT))) / ((float) (MAX_HEIGHT_IMPERIAL - MIN_HEIGHT_IMPERIAL))) + MIN_HEIGHT;
            if (mHeightValue < MIN_HEIGHT) {
                mHeightValue = MIN_HEIGHT;
            }

            mSbHeight.setMax(MAX_HEIGHT);
        } else {
            minWeight = (int) (MIN_WEIGHT * KG_TO_IBS);
            mWeightValue = (int) (mWeightValue * KG_TO_IBS);
            if (mWeightValue < (int) (MIN_WEIGHT * KG_TO_IBS)) {
                mWeightValue = (int) (MIN_WEIGHT * KG_TO_IBS);
            }
            mSbWeight.setMax((int) (MAX_WEIGHT * KG_TO_IBS));

            minHeight = MIN_HEIGHT_IMPERIAL;
            mHeightValue = (int) (((float) ((mHeightValue - MIN_HEIGHT) * (MAX_HEIGHT_IMPERIAL - MIN_HEIGHT_IMPERIAL))) / ((float) (MAX_HEIGHT - MIN_HEIGHT))) + MIN_HEIGHT_IMPERIAL;
            if (mHeightValue < MIN_HEIGHT_IMPERIAL) {
                mHeightValue = MIN_HEIGHT_IMPERIAL;
            }

            mSbHeight.setMax(MAX_HEIGHT_IMPERIAL);
        }

        mSbHeight.setProgress(getPercentProgress(minHeight, mSbHeight.getMax(), mHeightValue));
        mSbWeight.setProgress(getPercentProgress(minWeight, mSbWeight.getMax(), mWeightValue));
    }

    /**
     * Method sets all listeners in fragment.
     */
    private void setListeners() {
        setNextListener();
        setUnitsListeners();
        setSexListeners();
        setSeekBarsButtons();
        setAgeSeekBar();
        setWeightSeekBar();
        setHeightSeekBar();
    }

    /**
     * Method sets default sex to female.
     */
    private void setDefaultSex() {
        mLlFemale.setSelected(true);
        mIsMale = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_informations, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFonts();
        setText();
        setUnits(true);
        setToolbarTitle();
        setListeners();
        setDefaultSex();

        mActivity.setStep(1);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof RegisterActivity) {
            mActivity = (RegisterActivity) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
}
