package pl.happie.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.common.eventbus.Subscribe;

import pl.happie.HappieApplication;
import pl.happie.R;
import pl.happie.activities.MainActivity;
import pl.happie.events.AskMealEvent;
import pl.happie.events.OwnMealMenuEvent;
import pl.happie.fonts.RobotoLight;
import pl.happie.listeners.ChangeMealListener;
import roboguice.inject.InjectView;

/**
 * Fragment allows user to enter his
 * own custom meal.
 *
 * @author Tomasz Trybała
 */
public class OwnMealFragment extends RoboEventFragment {
    @InjectView(R.id.edtOwnMeal)
    private EditText mEdtMeal;

    private MainActivity mActivity;
    private boolean mIsCorrect;
    private ChangeMealListener mListener;

    public static OwnMealFragment getFragment(ChangeMealListener listener) {
        OwnMealFragment fragment = new OwnMealFragment();
        fragment.setListener(listener);

        return fragment;
    }

    private void setListener(ChangeMealListener listener) {
        mListener = listener;
    }

    /**
     * Method sets custom typeface to edit text view.
     */
    private void setFonts() {
        mEdtMeal.setTypeface(RobotoLight.getTypeface());
    }

    /**
     * Method sets correct toolbar title.
     */
    private void setToolbarTitle() {
        mActivity.setActionBarTitle(getResources().getString(R.string.own_meal_label));
    }

    /**
     * Method sets listeners to edit text view.
     */
    private void setListeners() {
        mEdtMeal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (TextUtils.isEmpty(mEdtMeal.getText()) && mIsCorrect) {
                    mIsCorrect = false;
                    HappieApplication.getEventBus().post(new OwnMealMenuEvent());
                } else if (!TextUtils.isEmpty(mEdtMeal.getText()) && !mIsCorrect) {
                    mIsCorrect = true;
                    HappieApplication.getEventBus().post(new OwnMealMenuEvent());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_own_meal, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFonts();
        setToolbarTitle();
        setListeners();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Subscribe
    public void sendText(final AskMealEvent event) {
        mListener.getMealName(mEdtMeal.getText().toString());
        mActivity.onBackPressed();
    }
}
