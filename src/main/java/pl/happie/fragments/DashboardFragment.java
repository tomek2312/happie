package pl.happie.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;

import pl.happie.R;
import pl.happie.activities.MainActivity;
import pl.happie.dialogs.MealFutureDialog;
import pl.happie.fonts.RobotoLight;
import pl.happie.views.FillImageView;
import roboguice.inject.InjectView;

/**
 * Main fragment in application.
 * Shows user his plan of meals
 * at current day.
 *
 * @author Tomasz Trybała
 */
public class DashboardFragment extends RoboEventFragment {
    @InjectView(R.id.rlDashboardActiveContainer)
    private RelativeLayout mRlActiveContainer;

    @InjectView(R.id.rlDashboardDietContainer)
    private RelativeLayout mRlDietContainer;

    @InjectView(R.id.rlDashboardRegularContainer)
    private RelativeLayout mRlRegularContainer;

    @InjectView(R.id.txtvDashboardActive)
    private TextView mTxtvActive;

    @InjectView(R.id.txtvDashboardDiet)
    private TextView mTxtvDiet;

    @InjectView(R.id.txtvDashboardRegular)
    private TextView mTxtvRegular;

    @InjectView(R.id.txtvDashboardHappie)
    private TextView mTxtvHappie;

    @InjectView(R.id.txtvDashboardSaddie)
    private TextView mTxtvSaddie;

    @InjectView(R.id.imgvDashboardActiveBg)
    private FillImageView mImgvActiveBg;

    @InjectView(R.id.imgvDashboardDietBg)
    private FillImageView mImgvDietBg;

    @InjectView(R.id.imgvDashboardRegularBg)
    private FillImageView mImgvRegularBg;

    @InjectView(R.id.imgvDashboardActiveTick)
    private ImageView mImgvActiveTick;

    @InjectView(R.id.imgvDashboardDietTick)
    private ImageView mImgvDietTick;

    @InjectView(R.id.imgvDashboardRegularTick)
    private ImageView mImgvRegularTick;

    @InjectView(R.id.llDashboardHappie)
    private LinearLayout mLlHappie;

    @InjectView(R.id.llDashboardSaddie)
    private LinearLayout mLlSaddie;

    private MainActivity mActivity;
    private boolean mIsAnimated;
    private Runnable onCompletedRunnable = new Runnable() {
        @Override
        public void run() {
            if (!mIsAnimated) {
                mIsAnimated = true;
                setDay(false);
            }
        }
    };

    private void setFonts() {
        mTxtvActive.setTypeface(RobotoLight.getTypeface());
        mTxtvDiet.setTypeface(RobotoLight.getTypeface());
        mTxtvRegular.setTypeface(RobotoLight.getTypeface());
        mTxtvHappie.setTypeface(RobotoLight.getTypeface());
        mTxtvSaddie.setTypeface(RobotoLight.getTypeface());
    }

    private void setListeners() {
        mTxtvActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                double d = Math.random();
//                Random random = new Random();
//                int value = d > 0.49 ? 100 : random.nextInt(100);
//                mImgvActiveBg.animateView(value, mImgvActiveTick, mImgvDietBg, mImgvRegularBg, onCompletedRunnable);

//                MealPastDialog dialog = MealPastDialog.getDialog(mActivity, "kolację", new ChangeMealListener() {
//                    @Override
//                    public void getMealName(String mealName) {
//                        Toast.makeText(mActivity, mealName, Toast.LENGTH_SHORT).show();
//                    }
//                });
//                dialog.show(mActivity.getSupportFragmentManager(), "dialog menu");

                MealFutureDialog dialog = MealFutureDialog.getDialog("kolacji");
                dialog.show(mActivity.getSupportFragmentManager(), "meal future dialog");
            }
        });

        mTxtvDiet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double d = Math.random();
                Random random = new Random();
                int value = d > 0.49 ? 100 : random.nextInt(100);
                mImgvDietBg.animateView(value, mImgvDietTick, mImgvActiveBg, mImgvRegularBg, onCompletedRunnable);
            }
        });

        mTxtvRegular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double d = Math.random();
                Random random = new Random();
                int value = d > 0.49 ? 100 : random.nextInt(100);
                mImgvRegularBg.animateView(value, mImgvRegularTick, mImgvDietBg, mImgvActiveBg, onCompletedRunnable);
            }
        });

        mLlHappie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCrystals();
            }
        });

        mLlSaddie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCrystals();
            }
        });
    }

    /**
     * Method returns relative left position of view.
     *
     * @param myView to calculate
     * @return left position
     */
    private int getRelativeLeft(View myView) {
        if (myView.getParent() == myView.getRootView())
            return myView.getLeft();
        else
            return myView.getLeft() + getRelativeLeft((View) myView.getParent());
    }

    /**
     * Method hides crystals and shows
     * happie or saddie day.
     *
     * @param isHappie if true sets happie day
     *                 else sets saddie day
     */
    private void setDay(final boolean isHappie) {
        int difference = getRelativeLeft(mRlDietContainer) - getRelativeLeft(mRlRegularContainer);

        PropertyValuesHolder pvhRight = PropertyValuesHolder.ofFloat("translationX", difference);
        PropertyValuesHolder pvhLeft = PropertyValuesHolder.ofFloat("translationX", -difference);

        ValueAnimator translateRegular = ObjectAnimator.ofPropertyValuesHolder(mRlRegularContainer,
                pvhRight);

        ValueAnimator translateActive = ObjectAnimator.ofPropertyValuesHolder(mRlActiveContainer,
                pvhLeft);

        ObjectAnimator alphaRegular = ObjectAnimator.ofFloat(mRlRegularContainer,
                "alpha", 1.0f, 0.0f);

        ObjectAnimator alphaDiet = ObjectAnimator.ofFloat(mRlDietContainer,
                "alpha", 1.0f, 0.0f);

        ObjectAnimator alphaActive = ObjectAnimator.ofFloat(mRlActiveContainer,
                "alpha", 1.0f, 0.0f);

        AnimatorSet set = new AnimatorSet();
        set.playTogether(translateActive, translateRegular, alphaActive, alphaDiet, alphaRegular);
        set.setDuration(800);
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                showView(isHappie ? mLlHappie : mLlSaddie).start();
            }
        });

        set.start();
    }

    /**
     * Method shows crystals to default positions.
     */
    private void setCrystals() {
        PropertyValuesHolder pvhRight = PropertyValuesHolder.ofFloat("translationX", 0f);
        PropertyValuesHolder pvhLeft = PropertyValuesHolder.ofFloat("translationX", 0.f);

        ValueAnimator translateRegular = ObjectAnimator.ofPropertyValuesHolder(mRlActiveContainer,
                pvhRight);

        ValueAnimator translateActive = ObjectAnimator.ofPropertyValuesHolder(mRlRegularContainer,
                pvhLeft);

        ObjectAnimator alphaRegular = ObjectAnimator.ofFloat(mRlRegularContainer,
                "alpha", 0.0f, 1.0f);

        ObjectAnimator alphaDiet = ObjectAnimator.ofFloat(mRlDietContainer,
                "alpha", 0.0f, 1.0f);

        ObjectAnimator alphaActive = ObjectAnimator.ofFloat(mRlActiveContainer,
                "alpha", 0.0f, 1.0f);

        final AnimatorSet set = new AnimatorSet();
        set.playTogether(translateActive, translateRegular, alphaActive, alphaDiet, alphaRegular);
        set.setDuration(800);

        View dayView = mLlHappie.getVisibility() == View.VISIBLE ? mLlHappie : mLlSaddie;
        ValueAnimator animator = hideView(dayView);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                set.start();

            }
        });

        animator.start();
    }

    /**
     * Method shows view by animation.
     *
     * @param view view to show
     * @return value animator with ready animation
     */
    private ValueAnimator showView(View view) {
        view.setAlpha(0.0f);
        view.setScaleX(0.0f);
        view.setScaleY(0.0f);
        view.setVisibility(View.VISIBLE);

        PropertyValuesHolder pvhAlpha = PropertyValuesHolder.ofFloat("alpha", 1.0f);
        PropertyValuesHolder pvhScaleX = PropertyValuesHolder.ofFloat("scaleX", 1.0f);
        PropertyValuesHolder pvhScaleY = PropertyValuesHolder.ofFloat("scaleY", 1.0f);
        final ValueAnimator happieAnimator = ObjectAnimator.ofPropertyValuesHolder(view,
                pvhAlpha, pvhScaleX, pvhScaleY);
        happieAnimator.setDuration(800);
        happieAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mIsAnimated = false;
            }
        });

        return happieAnimator;
    }

    /**
     * Method hides view by animation.
     *
     * @param view view to hide
     * @return value animator with ready animation
     */
    private ValueAnimator hideView(final View view) {
        view.setAlpha(1.0f);
        view.setScaleX(1.0f);
        view.setScaleY(1.0f);

        PropertyValuesHolder pvhAlpha = PropertyValuesHolder.ofFloat("alpha", 0.0f);
        PropertyValuesHolder pvhScaleX = PropertyValuesHolder.ofFloat("scaleX", 0.0f);
        PropertyValuesHolder pvhScaleY = PropertyValuesHolder.ofFloat("scaleY", 0.0f);
        final ValueAnimator happieAnimator = ObjectAnimator.ofPropertyValuesHolder(view,
                pvhAlpha, pvhScaleX, pvhScaleY);
        happieAnimator.setInterpolator(new AccelerateInterpolator());
        happieAnimator.setDuration(800);
        happieAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.INVISIBLE);
            }
        });

        return happieAnimator;
    }

    /**
     * Method sets correct toolbar title.
     */
    private void setToolbarTitle() {
        mActivity.setActionBarTitle(getResources().getString(R.string.app_name));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFonts();
        setListeners();
        setToolbarTitle();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
}
