package pl.happie.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import pl.happie.R;
import pl.happie.activities.LoginActivity;
import pl.happie.fonts.RobotoLight;
import pl.happie.tools.ValidationUtils;
import roboguice.inject.InjectView;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Fragment allows user to get email
 * with new password to his account.
 *
 *@author Tomasz Trybała
 */
public class ForgotPasswordFragment extends RoboEventFragment {
    private static final String EXTRA_EMAIL = "email";

    @InjectView(R.id.edtForgotPasswordEmail)
    private EditText mEdtEmail;

    @InjectView(R.id.txtvForgotPasswordInfo)
    private TextView mTxtvInfo;

    @InjectView(R.id.llForgotPasswordSend)
    private LinearLayout mLlSend;

    private LoginActivity mActivity;
    private String mEmail;

    /**
     * Method hides keyboard if is visible.
     */
    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);

        View view = mActivity.getCurrentFocus();
        if (view != null) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * Method sets custom font to views.
     */
    private void setFonts(){
        mTxtvInfo.setTypeface(RobotoLight.getTypeface());
    }

    /**
     * Method sets login button enabled
     * if form is valid or sets disabled
     * if form is invalid.
     *
     * @param isValidForm if form is valid
     */
    private void setLoginButton(boolean isValidForm){
        if(isValidForm){
            mLlSend.setBackgroundResource(R.drawable.intro_green_button);
            mLlSend.setEnabled(true);
        }else{
            mLlSend.setBackgroundResource(R.drawable.login_gray_button);
            mLlSend.setEnabled(false);
        }
    }

    /**
     * Method sets listeners to clickable views.
     */
    private void setListeners(){
        mEdtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                setLoginButton(ValidationUtils.isValidEmail(mEdtEmail.getText().toString()));
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mLlSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo send info about forgot password
            }
        });
    }

    /**
     * Method sets correct toolbar title.
     */
    private void setToolbarTitle(){
        mActivity.setActionBarTitle(getResources().getString(R.string.forgot_passwrod_title));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forgot_password, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbarTitle();
        setFonts();
        setListeners();
        mEdtEmail.setText(mEmail);
    }

    public static @NonNull
    ForgotPasswordFragment createInstance(@NonNull String email) {
        checkNotNull(email);

        Bundle extras = new Bundle();
        extras.putString(EXTRA_EMAIL, email);

        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        fragment.setArguments(extras);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getArguments();
        mEmail = extras.getString(EXTRA_EMAIL);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hideKeyboard();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof LoginActivity) {
            mActivity = (LoginActivity) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
}
