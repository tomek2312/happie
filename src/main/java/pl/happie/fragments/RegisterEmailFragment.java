package pl.happie.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pl.happie.BundleConstants;
import pl.happie.ConstantsAPI;
import pl.happie.HappieApplication;
import pl.happie.R;
import pl.happie.activities.MainActivity;
import pl.happie.activities.RegisterActivity;
import pl.happie.fonts.RobotoLight;
import pl.happie.net.model.ErrorResponse;
import pl.happie.net.requests.RegisterEmailRequest;
import pl.happie.tools.ValidationUtils;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
public class RegisterEmailFragment extends RoboEventFragment {
    private static final int ERROR_EMAIL_CONFLICT = 2;
    private static final int ERROR_DEVICE_LIMIT = 3;

    @InjectView(R.id.txtvRegisterEmailTitle)
    private TextView mTxtvTitle;

    @InjectView(R.id.edtRegisterEmailEmail)
    private EditText mEdtMail;

    @InjectView(R.id.edtRegisterEmailPassword)
    private EditText mEdtPassword;

    @InjectView(R.id.edtRegisterEmailName)
    private EditText mEdtName;

    @InjectView(R.id.llRegisterEmailJoin)
    private LinearLayout mLlJoin;

    @InjectView(R.id.imgvRegisterEmailVisibility)
    private ImageView mImgvPasswordVisibility;

    private RegisterActivity mActivity;
    private boolean mIsPasswordVisibility;

    /**
     * Method sets fonts.
     */
    private void setFonts() {
        mTxtvTitle.setTypeface(RobotoLight.getTypeface());
    }

    /**
     * Method sets text to title view from html.
     */
    private void setText() {
        mTxtvTitle.setText(Html.fromHtml(getResources().getString(R.string.registration_text5)));
    }

    /**
     * Method sets correct toolbar title.
     */
    private void setToolbarTitle() {
        mActivity.setActionBarTitle(getResources().getString(R.string.registration_title));
    }

    /**
     * Method allows user to show or hide his password.
     */
    private void changePasswordVisibility() {
        mIsPasswordVisibility = !mIsPasswordVisibility;
        int indexStart = mEdtPassword.getSelectionStart();
        int indexEnd = mEdtPassword.getSelectionEnd();

        if (mIsPasswordVisibility) {
            mImgvPasswordVisibility.setImageDrawable(
                    getResources().getDrawable(R.drawable.ic_password_show));
            mEdtPassword.setTransformationMethod(null);
        } else {
            mImgvPasswordVisibility.setImageDrawable(
                    getResources().getDrawable(R.drawable.ic_password_hide));
            mEdtPassword.setTransformationMethod(new PasswordTransformationMethod());
        }

        mEdtPassword.setSelection(indexStart, indexEnd);
    }

    /**
     * Method sets listeners to views.
     */
    private void setListeners() {
        mEdtMail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    if (ValidationUtils.isValidEmail(mEdtMail.getText().toString())) {
                        mEdtMail.setError(null);
                    } else {
                        mEdtMail.setError(getResources().getString(R.string.login_incorrect_email));
                    }
                }
            }
        });

        mEdtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                setLoginButton(isFormValid());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mEdtMail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                setLoginButton(isFormValid());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mEdtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                setLoginButton(isFormValid());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mLlJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendDataOnServer();
            }
        });

        mImgvPasswordVisibility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePasswordVisibility();
            }
        });
    }

    /**
     * Method sends data on server.
     */
    private void sendDataOnServer() {
        mActivity.showDialog();

        JSONObject object = RegisterEmailRequest.getJson(
                mEdtName.getText().toString(),
                mEdtMail.getText().toString(),
                mEdtPassword.getText().toString(),
                mActivity);

        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.PUT,
                ConstantsAPI.REGISTRATION_BY_EMAIL, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                saveToken(response);
                mActivity.hideDialog();

                Intent intent = new Intent(mActivity, MainActivity.class);
                startActivity(intent);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mActivity.hideDialog();
                        if (error instanceof NoConnectionError) {
                            mActivity.showToast(getResources().getString(R.string.register_data_internet_failure));
                        } else {
                            if (error.networkResponse != null && error.networkResponse.data != null) {
                                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                ErrorResponse weightTarget = gson.fromJson(new String(error.networkResponse.data),
                                        ErrorResponse.class);
                                if (weightTarget.getStatusCode() == ERROR_EMAIL_CONFLICT) {
                                    mActivity.showToast(getResources().getString(R.string.register_data_email_conflict));
                                } else if (weightTarget.getStatusCode() == ERROR_DEVICE_LIMIT) {
                                    mActivity.showToast(getResources().getString(R.string.register_data_device_limit));
                                } else {
                                    mActivity.showToast(getResources().getString(R.string.register_data_error_default));
                                }
                            } else {
                                mActivity.showToast(getResources().getString(R.string.register_data_error_default));
                            }
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put(RegisterActivity.TEMPORARY_TOKEN, mActivity.mTemporaryToken);
                return headers;
            }
        };

        HappieApplication.getInstance().getRequestQueue().add(objectRequest);
    }

    /**
     * Method saves user token to shared preferences
     *
     * @param jsonObject that contains token
     */
    private void saveToken(JSONObject jsonObject) {
        try {
            HappieApplication.saveToSettings(BundleConstants.PREF_ACCESS_LOGIN_TOKEN,
                    jsonObject.getJSONObject("data").getString("X-Access-Token"));
        } catch (JSONException e) {
            Toast.makeText(mActivity, "Error!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Method sets login button enabled
     * if form is valid or sets disabled
     * if form is invalid.
     *
     * @param isValidForm if form is valid
     */
    private void setLoginButton(boolean isValidForm) {
        if (isValidForm) {
            mLlJoin.setBackgroundResource(R.drawable.intro_green_button);
            mLlJoin.setEnabled(true);
        } else {
            mLlJoin.setBackgroundResource(R.drawable.login_gray_button);
            mLlJoin.setEnabled(false);
        }
    }

    /**
     * Method checks if mail pattern is valid
     * and if password is not empty.
     *
     * @return true     mail and password is valid
     * false    mail or passwords is invalid
     */
    private boolean isFormValid() {
        return ValidationUtils.isValidEmail(mEdtMail.getText().toString()) &&
                ValidationUtils.isValidPassword(mEdtPassword.getText().toString()) &&
                mEdtName.getText().length() > 3;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_email, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFonts();
        setText();
        setToolbarTitle();
        setListeners();

        mActivity.setStep(-1);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof RegisterActivity) {
            mActivity = (RegisterActivity) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
}
