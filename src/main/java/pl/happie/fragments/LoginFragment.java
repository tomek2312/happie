package pl.happie.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import pl.happie.BundleConstants;
import pl.happie.ConstantsAPI;
import pl.happie.HappieApplication;
import pl.happie.R;
import pl.happie.activities.LoginActivity;
import pl.happie.fonts.RobotoLight;
import pl.happie.net.PostLoginHelper;
import pl.happie.net.model.ErrorResponse;
import pl.happie.net.requests.RegisterEmailRequest;
import pl.happie.tools.ValidationUtils;
import roboguice.inject.InjectView;

/**
 * Activity allows user login to application
 * by Facebook or Email.
 *
 * @author Tomasz Trybała
 */
public class LoginFragment extends RoboEventFragment {
    private static final int ERROR_EMAIL_CONFLICT = 2;
    private static final int ERROR_DEVICE_LIMIT = 3;

    @InjectView(R.id.edtLoginEmail)
    private EditText mEdtMail;

    @InjectView(R.id.edtLoginPassword)
    private EditText mEdtPassword;

    @InjectView(R.id.txtvLoginForgot)
    private TextView mTxtvForgot;

    @InjectView(R.id.imgvLoginVisibility)
    private ImageView mImgvPasswordVisibility;

    @InjectView(R.id.llLoginLogin)
    private LinearLayout mLlLogin;

    @InjectView(R.id.rlLoginFacebook)
    private RelativeLayout mRlFacebook;

    @InjectView(R.id.txtvLoginOr)
    private TextView mTxtvOr;

    @InjectView(R.id.rlLoginPasswordInfo)
    private RelativeLayout mRlTick;

    @InjectView(R.id.txtvLoginOk)
    private TextView mTxtvOk;

    @InjectView(R.id.txtvLoginPasswordInfo)
    private TextView mTxtvPasswordInfo;

    private boolean mIsPasswordVisibility;
    private LoginActivity mActivity;
    private boolean mIsFirstFocus = true;

    /**
     * Method sets fonts.
     */
    private void setFonts() {
        mTxtvOr.setTypeface(RobotoLight.getTypeface());
        mTxtvPasswordInfo.setTypeface(RobotoLight.getTypeface());
    }

    /**
     * Method allows user to show or hide his password.
     */
    private void changePasswordVisibility() {
        mIsPasswordVisibility = !mIsPasswordVisibility;
        int indexStart = mEdtPassword.getSelectionStart();
        int indexEnd = mEdtPassword.getSelectionEnd();

        if (mIsPasswordVisibility) {
            mImgvPasswordVisibility.setImageDrawable(
                    getResources().getDrawable(R.drawable.ic_password_show));
            mEdtPassword.setTransformationMethod(null);
        } else {
            mImgvPasswordVisibility.setImageDrawable(
                    getResources().getDrawable(R.drawable.ic_password_hide));
            mEdtPassword.setTransformationMethod(new PasswordTransformationMethod());
        }

        mEdtPassword.setSelection(indexStart, indexEnd);
    }

    /**
     * Method sets listeners to clickable views.
     */
    private void setListeners() {
        mImgvPasswordVisibility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePasswordVisibility();
            }
        });

        mLlLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendDataOnServer();
            }
        });

        mRlFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PostLoginHelper mAuth = new PostLoginHelper(getActivity(), "");
                mAuth.showProgressDialog(getString(R.string.signing_up_via_facebook));
                mAuth.getFacebookAccessToken(new PostLoginHelper.TokenListener() {
                    @Override
                    public void onAccessTokenReceived(String accessToken) {
                        Log.e("dupa", "token: "+accessToken);
                    }

                    @Override
                    public void onError(Exception exception) {
                        Log.e("dupa", "error login");
                    }
                });
            }
        });

        mEdtMail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                setLoginButton(isFormValid());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mEdtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                setLoginButton(isFormValid());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mEdtMail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    if (ValidationUtils.isValidEmail(mEdtMail.getText().toString())) {
                        mEdtMail.setError(null);
                    } else {
                        mEdtMail.setError(getResources().getString(R.string.login_incorrect_email));
                    }
                }
            }
        });

        mEdtPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b && mIsFirstFocus) {
                    mRlTick.setVisibility(View.VISIBLE);
                    mIsFirstFocus = false;
                }
            }
        });

        mTxtvForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                mActivity.setFragment(
                        ForgotPasswordFragment.createInstance(mEdtMail.getText().toString()));
            }
        });

        mTxtvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRlTick.setVisibility(View.GONE);
            }
        });
    }

    /**
     * Method sends data on server.
     */
    private void sendDataOnServer() {
        mActivity.showDialog();

        JSONObject object = RegisterEmailRequest.getJson(
                mEdtMail.getText().toString(),
                mEdtPassword.getText().toString(),
                mActivity);

        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.PUT,
                ConstantsAPI.REGISTRATION_BY_EMAIL, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                saveToken(response);
                mActivity.hideDialog();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mActivity.hideDialog();
                        if (error instanceof NoConnectionError) {
                            mActivity.showToast(getResources().getString(R.string.register_data_internet_failure));
                        } else {
                            if (error.networkResponse != null && error.networkResponse.data != null) {
                                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                ErrorResponse weightTarget = gson.fromJson(new String(error.networkResponse.data),
                                        ErrorResponse.class);
                                if (weightTarget.getStatusCode() == ERROR_EMAIL_CONFLICT) {
                                    mActivity.showToast(getResources().getString(R.string.register_data_email_conflict));
                                } else if (weightTarget.getStatusCode() == ERROR_DEVICE_LIMIT) {
                                    mActivity.showToast(getResources().getString(R.string.register_data_device_limit));
                                } else {
                                    mActivity.showToast(getResources().getString(R.string.register_data_error_default));
                                }
                            } else {
                                mActivity.showToast(getResources().getString(R.string.register_data_error_default));
                            }
                        }
                    }
                });

        HappieApplication.getInstance().getRequestQueue().add(objectRequest);
    }

    /**
     * Method saves user token to shared preferences
     *
     * @param jsonObject that contains token
     */
    private void saveToken(JSONObject jsonObject){
        try {
            HappieApplication.saveToSettings(BundleConstants.PREF_ACCESS_LOGIN_TOKEN,
                    jsonObject.getJSONObject("data").getString("X-Access-Token"));
        } catch (JSONException e) {
            Toast.makeText(mActivity, "Error!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Method sets login button enabled
     * if form is valid or sets disabled
     * if form is invalid.
     *
     * @param isValidForm if form is valid
     */
    private void setLoginButton(boolean isValidForm) {
        if (isValidForm) {
            mLlLogin.setBackgroundResource(R.drawable.intro_green_button);
            mLlLogin.setEnabled(true);
        } else {
            mLlLogin.setBackgroundResource(R.drawable.login_gray_button);
            mLlLogin.setEnabled(false);
        }
    }

    /**
     * Method checks if mail pattern is valid
     * and if password is not empty.
     *
     * @return true     mail and password is valid
     * false    mail or passwords is invalid
     */
    private boolean isFormValid() {
        return ValidationUtils.isValidEmail(mEdtMail.getText().toString()) &&
                ValidationUtils.isValidPassword(mEdtPassword.getText().toString());
    }

    /**
     * Method sets correct toolbar title.
     */
    private void setToolbarTitle() {
        mActivity.setActionBarTitle(getResources().getString(R.string.login_title));
    }

    /**
     * Method hides keyboard if is visible.
     */
    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);

        View view = mActivity.getCurrentFocus();
        if (view != null) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setToolbarTitle();
        setFonts();
        setListeners();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof LoginActivity) {
            mActivity = (LoginActivity) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
}
