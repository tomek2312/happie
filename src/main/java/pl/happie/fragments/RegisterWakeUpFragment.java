package pl.happie.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import pl.happie.net.model.DietType;
import pl.happie.net.model.WeightTarget;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import pl.happie.ConstantsAPI;
import pl.happie.HappieApplication;
import pl.happie.R;
import pl.happie.activities.RegisterActivity;
import pl.happie.fonts.RobotoLight;
import pl.happie.fonts.RobotoMedium;
import pl.happie.net.requests.RegisterWakeUpRequest;
import roboguice.inject.InjectView;

/**
 * Fragment allows user to set his
 * wake up hours at week and weekend
 *
 * @author Tomasz Trybała
 */
public class RegisterWakeUpFragment extends RoboEventFragment {
    private static final int MAX_RANGE = 95;
    private static final int DEFAULT_WEEK = 34;
    private static final int DEFAULT_WEEKEND = 34;

    @InjectView(R.id.txtvRegisterWakeUpTitle)
    private TextView mTxtvTitle;

    @InjectView(R.id.txtvRegisterWakeUpWeek)
    private TextView mTxtvWeekValue;

    @InjectView(R.id.txtvRegisterWakeUpWeekend)
    private TextView mTxtvWeekendValue;

    @InjectView(R.id.imgvRegisterWakeUpWeekendMinus)
    private ImageView mImgvWeekendMinus;

    @InjectView(R.id.imgvRegisterWakeUpWeekMinus)
    private ImageView mImgvWeekMinus;

    @InjectView(R.id.imgvRegisterWakeUpWeekendPlus)
    private ImageView mImgvWeekendPlus;

    @InjectView(R.id.imgvRegisterWakeUpWeekPlus)
    private ImageView mImgvWeekPlus;

    @InjectView(R.id.sbRegisterWakeUpWeek)
    private SeekBar mSbWeek;

    @InjectView(R.id.sbRegisterWakeUpWeekend)
    private SeekBar mSbWeekend;

    @InjectView(R.id.llRegisterWakeUpNext)
    private LinearLayout mLlNext;

    private RegisterActivity mActivity;
    private int mWeekValue;
    private int mWeekendValue;
    private Integer mDiet = null;
    private Integer mWeight = null;

    /**
     * Method sets fonts.
     */
    private void setFonts() {
        mTxtvTitle.setTypeface(RobotoLight.getTypeface());
        mTxtvWeekendValue.setTypeface(RobotoMedium.getTypeface());
        mTxtvWeekValue.setTypeface(RobotoMedium.getTypeface());
    }

    /**
     * Method sets text to title view from html.
     */
    private void setText() {
        mTxtvTitle.setText(Html.fromHtml(getResources().getString(R.string.registration_text3)));
    }

    /**
     * Method sets correct toolbar title.
     */
    private void setToolbarTitle() {
        mActivity.setActionBarTitle(getResources().getString(R.string.register_step_3_title));
    }

    /**
     * Method sets listeners
     * to image views of minus and plus.
     */
    private void setSeekBarsButtons() {
        mImgvWeekMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSbWeek.setProgress(mSbWeek.getProgress() - 1);
            }
        });

        mImgvWeekendMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSbWeekend.setProgress(mSbWeekend.getProgress() - 1);
            }
        });

        mImgvWeekPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSbWeek.setProgress(mSbWeek.getProgress() + 1);
            }
        });

        mImgvWeekendPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSbWeekend.setProgress(mSbWeekend.getProgress() + 1);
            }
        });
    }

    /**
     * Method sets listeners to seek bars,
     * sets also max value and default value.
     */
    private void setSeekBarsListeners() {
        mSbWeek.setMax(MAX_RANGE);
        mSbWeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mWeekValue = progress;
                setValueText(mWeekValue, mTxtvWeekValue);
            }
        });

        mSbWeek.setProgress(DEFAULT_WEEK);
        setValueText(mWeekValue, mTxtvWeekValue);

        mSbWeekend.setMax(MAX_RANGE);
        mSbWeekend.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mWeekendValue = progress;
                setValueText(mWeekendValue, mTxtvWeekendValue);
            }
        });

        mSbWeekend.setProgress(DEFAULT_WEEKEND);
        setValueText(mWeekendValue, mTxtvWeekendValue);
    }

    /**
     * Method displays seek bar progress
     * in correct form.
     *
     * @param value    progress of seek bar
     * @param textView text view that displays value
     */
    private void setValueText(int value, TextView textView) {
        String stringValue = ((value / 4) < 10 ? "0" + (value / 4) : (value / 4)) + ":"
                + ((value % 4) == 0 ? "00" : ((value % 4) * 15) + "");
        textView.setText(stringValue);
    }

    /**
     * Method set listener to view that finishes
     * form.
     */
    private void setNextListener() {
        mLlNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPossibleWeights();
                getPossibleDietTypes();
            }
        });
    }

    /**
     * Method sends data on server and
     * receive possible weights for user.
     *
     */
    private void getPossibleWeights() {
        mActivity.showDialog();

        JSONObject object = RegisterWakeUpRequest.getJson(
                mTxtvWeekValue.getText().toString(),
                mTxtvWeekendValue.getText().toString(),
                mActivity);

        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.PUT,
                ConstantsAPI.REGISTRATION_STEP_3, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");
                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
                    WeightTarget weightTarget = gson.fromJson(data.toString(), WeightTarget.class);
                    mActivity.setWeightTarget(weightTarget);
                    mWeight = 1;
                    finishFragment();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NoConnectionError) {
                            mWeight = -2;
                        } else {
                            mWeight = -1;
                        }

                        finishFragment();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put(RegisterActivity.TEMPORARY_TOKEN, mActivity.mTemporaryToken);
                return headers;
            }
        };

        HappieApplication.getInstance().getRequestQueue().add(objectRequest);
    }

    private void finishFragment() {
        if (mDiet != null && mWeight != null) {
            if (mDiet > 0 && mWeight > 0) {
                mActivity.hideDialog();
                mActivity.setFragment(new RegisterTargetFragment());
            } else {
                mActivity.hideDialog();
                if (mDiet == -2 || mWeight == -2) {
                    mActivity.showToast(getResources().getString(R.string.register_data_internet_failure));
                } else {
                    mActivity.showToast(getResources().getString(R.string.register_data_error_default));
                }
            }

            mDiet = null;
            mWeight = null;
        }
    }

    /**
     * Method sends data on server and
     * receive possible diet types for user.
     */
    private void getPossibleDietTypes() {
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET,
                String.format(ConstantsAPI.REGISTRATION_DIET_TYPES, Locale.getDefault().getLanguage()),
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                DietType dietType = gson.fromJson(response.toString(), DietType.class);
                mActivity.setDietTypes(dietType);
                mDiet = 1;
                finishFragment();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NoConnectionError) {
                            mDiet = -2;
                        } else {
                            mDiet = -1;
                        }

                        finishFragment();
                    }
                });

        HappieApplication.getInstance().getRequestQueue().add(objectRequest);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_wake_up, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFonts();
        setText();
        setToolbarTitle();
        setSeekBarsButtons();
        setNextListener();
        setSeekBarsListeners();

        mActivity.setStep(3);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof RegisterActivity) {
            mActivity = (RegisterActivity) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
}
