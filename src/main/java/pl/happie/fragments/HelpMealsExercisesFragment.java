package pl.happie.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pl.happie.R;
import pl.happie.fonts.RobotoLight;
import roboguice.inject.InjectView;

/**
 * Fragment contains information about
 * meals & exercises.
 *
 *@author Tomasz Trybała
 */
public class HelpMealsExercisesFragment extends RoboEventFragment {
    @InjectView(R.id.txtvHelpMealsExercises1)
    private TextView mTxtv1;

    @InjectView(R.id.txtvHelpMealsExercises2)
    private TextView mTxtv2;

    @InjectView(R.id.txtvHelpMealsExercises3)
    private TextView mTxtv3;

    @InjectView(R.id.txtvHelpMealsExercises4)
    private TextView mTxtv4;

    @InjectView(R.id.txtvHelpMealsExercises5)
    private TextView mTxtv5;

    @InjectView(R.id.txtvHelpMealsExercises6)
    private TextView mTxtv6;

    /**
     * Method sets custom typeface to text views.
     */
    private void setFonts(){
        mTxtv1.setTypeface(RobotoLight.getTypeface());
        mTxtv2.setTypeface(RobotoLight.getTypeface());
        mTxtv3.setTypeface(RobotoLight.getTypeface());
        mTxtv4.setTypeface(RobotoLight.getTypeface());
        mTxtv5.setTypeface(RobotoLight.getTypeface());
        mTxtv6.setTypeface(RobotoLight.getTypeface());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_help_meals_exercises, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFonts();
    }
}
