package pl.happie.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pl.happie.ConstantsAPI;
import pl.happie.HappieApplication;
import pl.happie.R;
import pl.happie.activities.RegisterActivity;
import pl.happie.fonts.RobotoLight;
import pl.happie.fonts.RobotoMedium;
import pl.happie.net.requests.RegisterFigureRequest;
import pl.happie.views.ObservableHorizontalScrollView;
import roboguice.inject.InjectView;

/**
 * Fragment allows user to set
 * his current body type, wrist type and
 * body type when he was a child.
 * .
 *
 * @author Tomasz Trybała
 */
public class RegisterFigureFragment extends RoboEventFragment {
    private class BodyPart {
        String mName;
        int mDrawableId;
        boolean mIsFirstOrLast;

        BodyPart(String name, int drawableId, boolean isFirstOrLast) {
            mName = name;
            mDrawableId = drawableId;
            mIsFirstOrLast = isFirstOrLast;
        }

        String getName() {
            return mName;
        }

        int getDrawable() {
            return mDrawableId;
        }

        boolean isFirstOrLast() {
            return mIsFirstOrLast;
        }
    }

    @InjectView(R.id.txtvRegisterFigureTitle)
    private TextView mTxtvTitle;

    @InjectView(R.id.txtvRegisterFigureWristInfo)
    private TextView mTxtvWristInfo;

    @InjectView(R.id.hsvRegisterFigureWrist)
    private ObservableHorizontalScrollView mHsvWrist;

    @InjectView(R.id.llRegisterFigureWrist)
    private LinearLayout mLlWrist;

    @InjectView(R.id.txtvRegisterFigureWristType)
    private TextView mTxtvWristType;

    @InjectView(R.id.txtvRegisterFigureBodyInfo)
    private TextView mTxtvBodyInfo;

    @InjectView(R.id.hsvRegisterFigureBody)
    private ObservableHorizontalScrollView mHsvBody;

    @InjectView(R.id.llRegisterFigureBody)
    private LinearLayout mLlBody;

    @InjectView(R.id.txtvRegisterFigureBodyType)
    private TextView mTxtvBodyType;

    @InjectView(R.id.txtvRegisterFigureFormInfo)
    private TextView mTxtvFormInfo;

    @InjectView(R.id.txtvRegisterFigureFormTitle)
    private TextView mTxtvFormTitle;

    @InjectView(R.id.llRegisterFigureNext)
    private LinearLayout mLlNext;

    @InjectView(R.id.rbRegisterFigure1)
    private RadioButton mRbFirst;

    @InjectView(R.id.rbRegisterFigure2)
    private RadioButton mRbSecond;

    @InjectView(R.id.rbRegisterFigure3)
    private RadioButton mRbThird;

    private RegisterActivity mActivity;
    private final ArrayList<BodyPart> mWristTypes = new ArrayList<>();
    private final ArrayList<BodyPart> mBodyTypes = new ArrayList<>();
    private boolean mIsMale;
    private boolean mWasCreated;
    private View mCurrentWrist;
    private View mCurrentBody;

    /**
     * Method sets fonts.
     */
    private void setFonts() {
        mTxtvTitle.setTypeface(RobotoLight.getTypeface());
        mTxtvWristInfo.setTypeface(RobotoLight.getTypeface());
        mTxtvBodyInfo.setTypeface(RobotoLight.getTypeface());
        mTxtvFormInfo.setTypeface(RobotoLight.getTypeface());
        mRbFirst.setTypeface(RobotoLight.getTypeface());
        mRbSecond.setTypeface(RobotoLight.getTypeface());
        mRbThird.setTypeface(RobotoLight.getTypeface());
    }

    /**
     * Method sets strings to text view according to
     * current gender.
     */
    private void setForm() {
        mTxtvFormTitle.setText(getResources().getString(mIsMale ?
                R.string.child_type :
                R.string.child_type_female));
        mTxtvFormInfo.setText(getResources().getString(mIsMale ?
                R.string.child_type_description :
                R.string.child_type_description_female));
        mRbFirst.setText(getResources().getString(mIsMale ?
                R.string.child_type1 :
                R.string.child_type1_female));
        mRbSecond.setText(getResources().getString(mIsMale ?
                R.string.child_type2 :
                R.string.child_type2_female));
        mRbThird.setText(getResources().getString(mIsMale ?
                R.string.child_type3 :
                R.string.child_type3_female));
    }

    /**
     * Method sets listeners to radio buttons, that change typeface
     * according to check state.
     */
    private void setRadioButtonsListeners() {
        CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    compoundButton.setTypeface(RobotoMedium.getTypeface());
                } else {
                    compoundButton.setTypeface(RobotoLight.getTypeface());
                }
            }
        };

        mRbFirst.setOnCheckedChangeListener(listener);
        mRbSecond.setOnCheckedChangeListener(listener);
        mRbThird.setOnCheckedChangeListener(listener);

        mRbSecond.setChecked(true);
    }

    /**
     * Method sets text to title view from html.
     */
    private void setTitleText() {
        mTxtvTitle.setText(Html.fromHtml(getResources().getString(R.string.registration_text2)));
    }

    /**
     * Method sets correct toolbar title.
     */
    private void setToolbarTitle() {
        mActivity.setActionBarTitle(getResources().getString(R.string.register_step_2_title));
    }

    /**
     * Method animates view to center of scroll view.
     *
     * @param view view which will be animated
     */
    private void animateScroll(ObservableHorizontalScrollView scrollView, View view,
                               TextView textView) {
        int center = scrollView.getScrollX() + scrollView.getWidth() / 2;
        int viewLeft = view.getLeft();
        int viewWidth = view.getWidth();
        scrollView.smoothScrollBy((viewLeft + viewWidth / 2) - center, 0);
        textView.setText((String) view.getTag());
    }

    /**
     * Method set alpha 1.0f to center view
     * and 0.5f on other wrist drawables
     *
     * @param view view which is in center
     */
    private void setAlphaOnViews(View view, LinearLayout layout) {
        if (layout == mLlBody) {
            mCurrentBody = view;
        } else {
            mCurrentWrist = view;
        }

        int childCount = layout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View v = layout.getChildAt(i);
            if (v.getTag().equals(view.getTag())) {
                v.setAlpha(1.0f);
            } else {
                v.setAlpha(0.5f);
            }
        }
    }

    /**
     * Method adds body types object to lists.
     */
    private void initList() {
        mWristTypes.clear();

        mWristTypes.add(new BodyPart("", R.drawable.wrist_ecto, true));
        mWristTypes.add(new BodyPart(getResources().getString(R.string.wrist_type1),
                R.drawable.wrist_ecto, false));
        mWristTypes.add(new BodyPart(getResources().getString(R.string.wrist_type2),
                R.drawable.wrist_meso, false));
        mWristTypes.add(new BodyPart(getResources().getString(R.string.wrist_type3),
                R.drawable.wrist_endo, false));
        mWristTypes.add(new BodyPart("", R.drawable.wrist_ecto, true));

        mBodyTypes.clear();

        mBodyTypes.add(new BodyPart("", R.drawable.body_ecto_male, true));
        mBodyTypes.add(new BodyPart(getResources().getString(R.string.body_type1),
                mIsMale ? R.drawable.body_ecto_male
                        : R.drawable.body_ecto_female, false));
        mBodyTypes.add(new BodyPart(getResources().getString(R.string.body_type2),
                mIsMale ? R.drawable.body_meso_male
                        : R.drawable.body_meso_female, false));
        mBodyTypes.add(new BodyPart(getResources().getString(R.string.body_type3),
                mIsMale ? R.drawable.body_endo_male
                        : R.drawable.body_endo_female, false));
        mBodyTypes.add(new BodyPart("", R.drawable.body_ecto_male, true));
    }

    /**
     * Method adds listener to layout
     * that finishes fragment.
     */
    private void setNextListener() {
        mLlNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendDataOnServer();
            }
        });
    }

    /**
     * Method sends data on server.
     */
    private void sendDataOnServer() {
        mActivity.showDialog();

        JSONObject object = RegisterFigureRequest.getJson(
                getCurrentWristId(),
                getCurrentBodyId(),
                getCurrentBodyHistory(),
                mActivity);

        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.PUT,
                ConstantsAPI.REGISTRATION_STEP_2, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mActivity.setFragment(new RegisterWakeUpFragment());
                mActivity.hideDialog();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mActivity.hideDialog();
                        if (error instanceof NoConnectionError) {
                            mActivity.showToast(getResources().getString(R.string.register_data_internet_failure));
                        } else {
                            mActivity.showToast(getResources().getString(R.string.register_data_error_default));
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put(RegisterActivity.TEMPORARY_TOKEN, mActivity.mTemporaryToken);
                return headers;
            }
        };

        HappieApplication.getInstance().getRequestQueue().add(objectRequest);
    }

    private int getCurrentWristId() {
        if (mCurrentWrist.getTag().equals(getResources().getString(R.string.wrist_type1))) {
            return 1;
        } else if (mCurrentWrist.getTag().equals(getResources().getString(R.string.wrist_type2))) {
            return 2;
        } else {
            return 3;
        }
    }

    private int getCurrentBodyId() {
        if (mCurrentBody.getTag().equals(getResources().getString(R.string.body_type1))) {
            return 1;
        } else if (mCurrentBody.getTag().equals(getResources().getString(R.string.body_type2))) {
            return 2;
        } else {
            return 3;
        }
    }

    private int getCurrentBodyHistory() {
        if (mRbFirst.isChecked()) {
            return 1;
        } else if (mRbSecond.isChecked()) {
            return 2;
        } else {
            return 3;
        }
    }

    /**
     * Method adds images to layouts
     * in scroll views.
     */
    private void addImageViewsToScroll() {
        for (BodyPart item : mWristTypes) {
            ImageView imageView = new ImageView(HappieApplication.getAppContext());
            imageView.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            imageView.setImageResource(item.getDrawable());
            imageView.setTag(item.getName());
            if (item.isFirstOrLast()) {
                imageView.setVisibility(View.INVISIBLE);
            }

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    animateScroll(mHsvWrist, view, mTxtvWristType);
                    setAlphaOnViews(view, mLlWrist);
                }
            });

            mLlWrist.addView(imageView);
        }

        for (BodyPart item : mBodyTypes) {
            ImageView imageView = new ImageView(HappieApplication.getAppContext());
            imageView.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            imageView.setImageResource(item.getDrawable());
            imageView.setTag(item.getName());
            if (item.isFirstOrLast()) {
                imageView.setVisibility(View.INVISIBLE);
            }

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    animateScroll(mHsvBody, view, mTxtvBodyType);
                    setAlphaOnViews(view, mLlBody);
                }
            });

            mLlBody.addView(imageView);
        }
    }

    /**
     * Method adds listeners to
     * scroll views.
     */
    private void setScrollListeners() {
        mHsvWrist.setSmoothScrollingEnabled(true);
        mHsvBody.setSmoothScrollingEnabled(true);


        mHsvWrist.setOnScrollListener(new ObservableHorizontalScrollView.OnScrollListener() {
            @Override
            public void onScrollChanged(ObservableHorizontalScrollView scrollView, int x, int y, int oldX, int oldY) {
            }

            @Override
            public void onEndScroll(ObservableHorizontalScrollView scrollView) {
                int center = mHsvWrist.getScrollX() + mHsvWrist.getWidth() / 2;
                int childCount = mLlWrist.getChildCount();
                View currentView = new View(mActivity);
                for (int i = 0; i < childCount; i++) {
                    View v = mLlWrist.getChildAt(i);
                    int viewLeft = v.getLeft();
                    int viewWidth = v.getWidth();
                    if (center >= viewLeft && center <= viewLeft + viewWidth) {
                        if (i == 0) {
                            v = mLlWrist.getChildAt(i + 1);
                        } else if (i == childCount - 1) {
                            v = mLlWrist.getChildAt(i - 1);
                        }
                        currentView = v;
                        animateScroll(mHsvWrist, currentView, mTxtvWristType);
                        break;
                    }
                }

                setAlphaOnViews(currentView, mLlWrist);
            }
        });

        mHsvBody.setOnScrollListener(new ObservableHorizontalScrollView.OnScrollListener() {
            @Override
            public void onScrollChanged(ObservableHorizontalScrollView scrollView, int x, int y, int oldX, int oldY) {
            }

            @Override
            public void onEndScroll(ObservableHorizontalScrollView scrollView) {
                int center = mHsvBody.getScrollX() + mHsvBody.getWidth() / 2;
                int childCount = mLlBody.getChildCount();
                View currentView = new View(mActivity);
                for (int i = 0; i < childCount; i++) {
                    View v = mLlBody.getChildAt(i);
                    int viewLeft = v.getLeft();
                    int viewWidth = v.getWidth();
                    if (center >= viewLeft && center <= viewLeft + viewWidth) {
                        if (i == 0) {
                            v = mLlBody.getChildAt(i + 1);
                        } else if (i == childCount - 1) {
                            v = mLlBody.getChildAt(i - 1);
                        }
                        currentView = v;
                        animateScroll(mHsvBody, currentView, mTxtvBodyType);
                        break;
                    }
                }

                setAlphaOnViews(currentView, mLlBody);
            }
        });
    }

    /**
     * Method sets second element in center on fragment start.
     * When fragment was created, method sets last values.
     */
    private void setDefaultPosition() {
        if (!mWasCreated) {
            mWasCreated = true;
            final ViewTreeObserver observer = mHsvWrist.getViewTreeObserver();
            if (observer != null && observer.isAlive()) {
                observer.addOnGlobalLayoutListener(
                        new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                int centerWrist = mHsvWrist.getScrollX() + mHsvWrist.getWidth() / 2;
                                View vWrist = mLlWrist.getChildAt(2);
                                int viewLeftWrist = vWrist.getLeft();
                                int viewWrist = vWrist.getWidth();
                                mHsvWrist.scrollTo((viewLeftWrist + viewWrist / 2) - centerWrist, 0);
                                mTxtvWristType.setText((String) vWrist.getTag());
                                setAlphaOnViews(vWrist, mLlWrist);

                                int centerBody = mHsvBody.getScrollX() + mHsvBody.getWidth() / 2;
                                View vBody = mLlBody.getChildAt(2);
                                int viewLeftBody = vBody.getLeft();
                                int viewBody = vBody.getWidth();
                                mHsvBody.scrollTo((viewLeftBody + viewBody / 2) - centerBody, 0);
                                mTxtvBodyType.setText((String) vBody.getTag());
                                setAlphaOnViews(vBody, mLlBody);

                                if (observer.isAlive()) {
                                    //noinspection deprecation
                                    observer.removeGlobalOnLayoutListener(this);
                                }
                            }
                        }
                );
            }
        } else {
            if (mCurrentBody != null) {
                setAlphaOnViews(mCurrentBody, mLlBody);
                mTxtvBodyType.setText((String) mCurrentBody.getTag());
            }

            if (mCurrentWrist != null) {
                setAlphaOnViews(mCurrentWrist, mLlWrist);
                mTxtvWristType.setText((String) mCurrentWrist.getTag());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mIsMale = mActivity.isMale();
        return inflater.inflate(R.layout.fragment_register_figure, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFonts();
        setTitleText();
        setForm();
        setToolbarTitle();
        initList();
        addImageViewsToScroll();
        setScrollListeners();
        setDefaultPosition();
        setNextListener();
        setRadioButtonsListeners();

        mActivity.setStep(2);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof RegisterActivity) {
            mActivity = (RegisterActivity) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
}
