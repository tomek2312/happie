package pl.happie.fragments;

import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import pl.happie.R;
import pl.happie.fonts.RobotoLight;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Fragment with information for user in IntroActivity.
 *
 *@author Tomasz Trybała
 */
public class IntroFragment extends RoboFragment{
    private static final String EXTRA_IMAGE = "image";
    private static final String EXTRA_TITLE = "title";
    private static final String EXTRA_CONTENT = "content";
    private static final String EXTRA_COLOR = "color";

    @InjectView(R.id.imgvIntroFragment)
    private ImageView mImageView;

    @InjectView(R.id.txtvIntroTitle)
    private TextView mTxtvTitle;

    @InjectView(R.id.txtvIntroContent)
    private TextView mTxtvContent;

    private int mImgId;
    private int mColorId;
    private String mTitle;
    private String mContent;

    private void setFonts(){
        mTxtvContent.setTypeface(RobotoLight.getTypeface());
        mTxtvTitle.setTypeface(RobotoLight.getTypeface());
    }

    public static @NonNull
    IntroFragment createInstance(@DrawableRes int imgId, @NonNull String title,
                                 @NonNull String content, @ColorRes int colorId) {
        checkNotNull(title);

        Bundle extras = new Bundle();
        extras.putInt(EXTRA_IMAGE, imgId);
        extras.putInt(EXTRA_COLOR, colorId);
        extras.putString(EXTRA_TITLE, title);
        extras.putString(EXTRA_CONTENT, content);

        IntroFragment fragment = new IntroFragment();
        fragment.setArguments(extras);

        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_intro, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFonts();

        mImageView.setImageResource(mImgId);
        mTxtvTitle.setText(mTitle);
        mTxtvTitle.setTextColor(mColorId);
        mTxtvContent.setText(Html.fromHtml(mContent));

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getArguments();
        mImgId = extras.getInt(EXTRA_IMAGE);
        mTitle = extras.getString(EXTRA_TITLE);
        mContent = extras.getString(EXTRA_CONTENT);
        mColorId = extras.getInt(EXTRA_COLOR);
    }
}
