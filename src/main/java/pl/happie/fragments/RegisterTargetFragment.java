package pl.happie.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pl.happie.ConstantsAPI;
import pl.happie.HappieApplication;
import pl.happie.R;
import pl.happie.activities.RegisterActivity;
import pl.happie.fonts.RobotoLight;
import pl.happie.fonts.RobotoMedium;
import pl.happie.net.model.DietTypeItem;
import pl.happie.net.model.WeightTargetEntry;
import pl.happie.net.requests.RegisterTargetRequest;
import roboguice.inject.InjectView;

/**
 * Fragment allows user to set his target weight
 * and type of diet. Finally user joins to app
 * by G+, Facebook or Email.
 *
 * @author Tomasz Trybała
 */
public class RegisterTargetFragment extends RoboEventFragment {
    private static int MIN_WEIGHT = 50;
    private static int DEFAULT_WEIGHT = 65;
    private static int MAX_WEIGHT = 80;

    @InjectView(R.id.txtvRegisterTargetTitle)
    private TextView mTxtvTitle;

    @InjectView(R.id.imgvRegisterTargetMinus)
    private ImageView mImgvMinus;

    @InjectView(R.id.imgvRegisterTargetPlus)
    private ImageView mImgvPlus;

    @InjectView(R.id.sbRegisterTarget)
    private SeekBar mSbTarget;

    @InjectView(R.id.txtvRegisterTargetTime)
    private TextView mTxtvTime;

    @InjectView(R.id.txtvRegisterTargetWeight)
    private TextView mTxtvWeight;

    @InjectView(R.id.rlRegisterTargetDietGroup)
    private RadioGroup mRgDiet;

    @InjectView(R.id.llRegisterTargetEmail)
    private LinearLayout mLlEmail;

    @InjectView(R.id.rlRegisterTargetFacebook)
    private RelativeLayout mRlFacebook;

    private HashMap<Integer, Integer> mWeeks = new HashMap<>();
    private RegisterActivity mActivity;
    private int mWeightValue;
    private String mCurrentUnit;
    private RadioButton mCurrentRadioButton;

    /**
     * Method sets values that came from server.
     */
    private void setValuesFromServer() {
        mCurrentUnit = getResources().getString(mActivity.isMetric() ?
                R.string.kg :
                R.string.lb);
        MIN_WEIGHT = mActivity.getWeightTarget().getMinWeight().getWeight();
        MAX_WEIGHT = mActivity.getWeightTarget().getMaxWeight().getWeight();
        DEFAULT_WEIGHT = mActivity.getDefaultWeight();
        for (WeightTargetEntry entry : mActivity.getWeightTarget().getEntryList()) {
            mWeeks.put(entry.getWeight().getWeight(), entry.getWeeks());
        }
    }

    /**
     * Method sets fonts.
     */
    private void setFonts() {
        mTxtvTitle.setTypeface(RobotoLight.getTypeface());
    }

    /**
     * Method sets text to title view from html.
     */
    private void setText() {
        mTxtvTitle.setText(Html.fromHtml(getResources().getString(R.string.registration_text4,
                MIN_WEIGHT,
                MAX_WEIGHT,
                mCurrentUnit)));
    }

    /**
     * Method sets correct toolbar title.
     */
    private void setToolbarTitle() {
        mActivity.setActionBarTitle(getResources().getString(R.string.register_step_4_title));
    }

    /**
     * Method sets listeners to change diet type.
     */
    private void setDietListeners() {
        for (DietTypeItem dietType : mActivity.getDietTypes().getEntryList()) {
            mRgDiet.addView(getRadioButton(dietType.getName(), dietType.getDietCode()));
        }

        RadioButton first = (RadioButton) mRgDiet.getChildAt(0);
        if (first != null) {
            first.setChecked(true);
        }
    }

    /**
     * Method sets listeners
     * to image views of minus and plus.
     */
    private void setSeekBarsButtons() {
        mImgvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSbTarget.setProgress(mSbTarget.getProgress() + 1);
            }
        });

        mImgvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSbTarget.setProgress(mSbTarget.getProgress() - 1);
            }
        });
    }

    private RadioButton getRadioButton(String text, int code) {
        int paddingLarge = (int) getResources().getDimension(R.dimen.padding_large);
        RadioButton radioButton = new RadioButton(mActivity);
        radioButton.setTag(code);
        radioButton.setButtonDrawable(getResources().getDrawable(R.drawable.register_figure_radio_button));
        radioButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        radioButton.setText(text);
        radioButton.setPadding(paddingLarge, 0, 0, 0);
        radioButton.setTypeface(RobotoLight.getTypeface());

        CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    compoundButton.setTypeface(RobotoMedium.getTypeface());
                    compoundButton.setTextColor(getResources().getColor(R.color.main_green));
                    mCurrentRadioButton = (RadioButton) compoundButton;
                } else {
                    compoundButton.setTypeface(RobotoLight.getTypeface());
                    compoundButton.setTextColor(getResources().getColor(R.color.toolbar_label));
                }
            }
        };

        radioButton.setOnCheckedChangeListener(listener);

        return radioButton;
    }


    /**
     * Method sets listener to seek bar of weight,
     * sets also max value and default value.
     */
    private void setWeightSeekBar() {
        mSbTarget.setMax(MAX_WEIGHT - MIN_WEIGHT);
        mSbTarget.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mWeightValue = progress + MIN_WEIGHT;
                setProgressOnViews();
            }
        });

        mWeightValue = DEFAULT_WEIGHT;
        mSbTarget.setProgress(mWeightValue - MIN_WEIGHT);
        setProgressOnViews();
    }

    /**
     * Method sets listeners to buttons.
     */
    private void setButtonsListeners() {
        mLlEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendDataOnServer(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.setFragment(new RegisterEmailFragment());
                    }
                });
            }
        });

        mRlFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendDataOnServer(new Runnable() {
                    @Override
                    public void run() {
                        //todo login by fb
                    }
                });
            }
        });
    }

    /**
     * Method sends data on server.
     */
    private void sendDataOnServer(final Runnable success) {
        mActivity.showDialog();

        JSONObject object = RegisterTargetRequest.getJson(mWeightValue, mActivity.isMetric(),
                (int)mCurrentRadioButton.getTag(), mActivity);


        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.PUT,
                ConstantsAPI.REGISTRATION_STEP_4, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                success.run();
                mActivity.hideDialog();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mActivity.hideDialog();
                        if (error instanceof NoConnectionError) {
                            mActivity.showToast(getResources().getString(R.string.register_data_internet_failure));
                        } else {
                            mActivity.showToast(getResources().getString(R.string.register_data_error_default));
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put(RegisterActivity.TEMPORARY_TOKEN, mActivity.mTemporaryToken);
                return headers;
            }
        };

        HappieApplication.getInstance().getRequestQueue().add(objectRequest);
    }

    /**
     * Method sets correctly contents in text views
     * according to weight value.
     */
    private void setProgressOnViews() {
        Integer weeks = mWeeks.get(mWeightValue);
        if (weeks != null) {
            if (mWeightValue == DEFAULT_WEIGHT) {
                mTxtvWeight.setText(getResources().getString(R.string.target_title_keep,
                        mWeightValue,
                        mCurrentUnit));
                mTxtvTime.setText("");
            } else if (mWeightValue < DEFAULT_WEIGHT) {
                mTxtvWeight.setText(getResources().getString(R.string.target_title_loose,
                        mWeightValue,
                        mCurrentUnit));
                mTxtvTime.setText(getResources().getString(R.string.target_subtitle) + weeks + " tygodnie");
            } else {
                mTxtvWeight.setText(getResources().getString(R.string.target_title_gain,
                        mWeightValue,
                        mCurrentUnit));
                mTxtvTime.setText(getResources().getString(R.string.target_subtitle) + weeks + " tygodnie");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_target, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setValuesFromServer();
        setFonts();
        setText();
        setToolbarTitle();
        setSeekBarsButtons();
        setDietListeners();
        setWeightSeekBar();
        setButtonsListeners();

        mActivity.setStep(4);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof RegisterActivity) {
            mActivity = (RegisterActivity) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
}
