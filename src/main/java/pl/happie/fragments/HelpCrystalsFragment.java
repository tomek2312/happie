package pl.happie.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pl.happie.R;
import pl.happie.fonts.RobotoLight;
import roboguice.inject.InjectView;

/**
 * Fragment contains information about
 * meals & exercises.
 *
 *@author Tomasz Trybała
 */
public class HelpCrystalsFragment extends RoboEventFragment{
    @InjectView(R.id.txtvHelpCrystalsDesc1)
    private TextView mTxtvDesc1;

    @InjectView(R.id.txtvHelpCrystalsDesc2)
    private TextView mTxtvDesc2;

    @InjectView(R.id.txtvHelpCrystalsDesc3)
    private TextView mTxtvDesc3;

    @InjectView(R.id.txtvHelpCrystalsDesc4)
    private TextView mTxtvDesc4;

    /**
     * Method sets custom typeface to text views.
     */
    private void setFonts(){
        mTxtvDesc1.setTypeface(RobotoLight.getTypeface());
        mTxtvDesc2.setTypeface(RobotoLight.getTypeface());
        mTxtvDesc3.setTypeface(RobotoLight.getTypeface());
        mTxtvDesc4.setTypeface(RobotoLight.getTypeface());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_help_crystals, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFonts();
    }
}
