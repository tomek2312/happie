package pl.happie.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Custom view, that animates and shows current
 * value of user progress.
 *
 * @author Tomasz Trybała
 */
public class FillImageView extends ImageView {
    //region variables
    private boolean mWasSet;
    private int mBottom;
    private int mMaxHeight;
    private int mCurrentHeight;
    private int mProgress;
    private boolean mIsAnimated;
    //endregion

    /**
     * Method animates tick view according to
     * current progress.
     *
     * @param tickView view with tick image
     */
    private void animateTick(@NonNull View tickView, final FillImageView first, final FillImageView second, final Runnable onCompleted) {
        if (mCurrentHeight == mMaxHeight) {
            if (tickView.getAlpha() != 1.0f) {
                tickView.setAlpha(0.0f);
                tickView.setScaleX(0.0f);
                tickView.setScaleY(0.0f);

                PropertyValuesHolder pvhAlpha = PropertyValuesHolder.ofFloat("alpha", 1.0f);
                PropertyValuesHolder pvhScaleX = PropertyValuesHolder.ofFloat("scaleX", 1.0f);
                PropertyValuesHolder pvhScaleY = PropertyValuesHolder.ofFloat("scaleY", 1.0f);
                ValueAnimator animator = ObjectAnimator.ofPropertyValuesHolder(tickView,
                        pvhAlpha, pvhScaleX, pvhScaleY);
                animator.setInterpolator(new OvershootInterpolator());
                animator.setDuration(400);
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mIsAnimated = false;
                        if (isCompleted() && first.isCompleted() && second.isCompleted()) {
                            onCompleted.run();
                        }
                    }
                });
                animator.start();

            } else {
                mIsAnimated = false;
            }
        } else if (tickView.getAlpha() == 1.0f) {
            PropertyValuesHolder pvhAlpha = PropertyValuesHolder.ofFloat("alpha", 0.0f);
            PropertyValuesHolder pvhScaleX = PropertyValuesHolder.ofFloat("scaleX", 0.0f);
            PropertyValuesHolder pvhScaleY = PropertyValuesHolder.ofFloat("scaleY", 0.0f);
            ValueAnimator animator = ObjectAnimator.ofPropertyValuesHolder(tickView,
                    pvhAlpha, pvhScaleX, pvhScaleY);
            animator.setInterpolator(new OvershootInterpolator());
            animator.setDuration(400);
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mIsAnimated = false;
                    if (isCompleted() && first.isCompleted() && second.isCompleted()) {
                        onCompleted.run();
                    }
                }
            });
            animator.start();
        }else{
            mIsAnimated = false;
        }
    }

    //region default constructors
    public FillImageView(Context context) {
        super(context);
    }

    public FillImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FillImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    //endregion

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        mBottom = bottom;
        mMaxHeight = bottom - top;
        if (!mWasSet) {
            mWasSet = true;
            this.setVisibility(INVISIBLE);
        }
        super.onLayout(changed, left, top, right, bottom);
    }

    /**
     * Method animates current view.
     *
     * @param progress target progress
     * @param tick     tick view
     */
    public void animateView(int progress, final View tick, final FillImageView first, final FillImageView second, final Runnable onCompleted) {
        checkArgument(progress >= 0, "progress must be not less than 0");
        checkArgument(progress <= 100, "progress must be not greater than 100");

        if (!mIsAnimated) {
            mIsAnimated = true;
            mProgress = progress;

            if (getVisibility() == INVISIBLE) {
                setTop(mBottom);
                setVisibility(VISIBLE);
            }

            final int height = (int) (((float) progress / 100.0f) * (float) mMaxHeight);
            int difference = height - mCurrentHeight;

            ValueAnimator animator = ObjectAnimator.ofInt(0, difference);
            animator.setDuration(800);
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    FillImageView.this.setTop(mBottom - mCurrentHeight - (int) animation.getAnimatedValue());
                }
            });

            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mCurrentHeight = height;
                    animateTick(tick, first, second, onCompleted);
                }
            });

            animator.start();
        }
    }

    /**
     * Method returns if exercise is completed.
     *
     * @return true when progress is 100
     */
    public boolean isCompleted() {
        return mProgress == 100;
    }
}
