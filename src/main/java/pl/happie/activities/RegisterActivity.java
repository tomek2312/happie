package pl.happie.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import pl.happie.R;
import pl.happie.RoboEventToolbarActivity;
import pl.happie.fragments.RegisterInfoFragment;
import pl.happie.net.model.DietType;
import pl.happie.net.model.WeightTarget;
import roboguice.inject.ContentView;

/**
 * Activity allows user to set body parameters,
 * target weight, wake up hours and body type.
 *
 * @author Tomasz Trybała
 */
@ContentView(R.layout.activity_register)
public class RegisterActivity extends RoboEventToolbarActivity {
    public static final String TEMPORARY_TOKEN = "X-Temporary-Token";
    public static final String TEMPORARY_DATA = "data";
    private static final int STEPS_NUMBER = 4;
    private DietType mDietType;
    private WeightTarget mWeightTarget;
    private int mDefaultWeight;
    private View mViewMenu;
    private TextView mTxtvMenuStep;
    private ProgressDialog mDialog;
    public String mTemporaryToken;
    private boolean mIsMale;
    private boolean mIsMetric;

    /**
     * Method finds view, that shows user number of step.
     */
    @SuppressLint("InflateParams")
    private void findViews() {
        LayoutInflater inflater = LayoutInflater.from(this);
        mViewMenu = inflater.inflate(R.layout.view_menu_step, null, false);
        mTxtvMenuStep = (TextView) mViewMenu.findViewById(R.id.txtvMenuStep);
    }

    /**
     * Method sets progress dialog, which is shown
     * when data from server is loading.
     */
    private void setProgressDialog() {
        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Loading...");
        mDialog.setCancelable(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findViews();
        setProgressDialog();
        setFragment(new RegisterInfoFragment());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_step, menu);

        MenuItem item = menu.findItem(R.id.action_step);
        item.setActionView(mViewMenu);

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Method sets current step in Toolbar.
     *
     * @param step current current step
     */
    public void setStep(int step) {
        if (step > 0) {
            mTxtvMenuStep.setText(getResources().
                    getString(R.string.register_step_pattern, step, STEPS_NUMBER));
        } else {
            mTxtvMenuStep.setText("");
        }
    }

    /**
     * Method sets temporary token, which allows user
     * to complete registration flow.
     *
     * @param token temporary token
     */
    public void setTemporaryToken(String token) {
        mTemporaryToken = token;
    }

    /**
     * Method shows progress dialog.
     */
    public void showDialog() {
        mDialog.show();
    }

    /**
     * Method hides progress dialog.
     */
    public void hideDialog() {
        if (mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    /**
     * Method shows toast.
     *
     * @param message message to user.
     */
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Method sets if user is man or female.
     *
     * @param isMale if user is male.
     */
    public void setGender(boolean isMale) {
        mIsMale = isMale;
    }

    /**
     * Method returns if current unit is metric
     * or imperial.
     *
     * @return true when unit is metric,
     * false when unit is imperial
     */
    public boolean isMetric() {
        return mIsMetric;
    }

    /**
     * Method sets current unit.
     *
     * @param isMetric if unit is metric.
     */
    public void setMetric(boolean isMetric) {
        mIsMetric = isMetric;
    }

    /**
     * Method returns sex of user.
     *
     * @return true when user is male,
     * false when user is female
     */
    public boolean isMale() {
        return mIsMale;
    }

    /**
     * Method sets available diet types.
     *
     * @param dietType available diet types.
     */
    public void setDietTypes(DietType dietType) {
        mDietType = dietType;
    }

    /**
     * Method returns available diet types.
     *
     * @return mDietType
     */
    public DietType getDietTypes() {
        return mDietType;
    }

    /**
     * Method sets information of
     * possible weights for user.
     *
     * @param target custom object with information
     */
    public void setWeightTarget(WeightTarget target){
        mWeightTarget = target;
    }

    /**
     * Method returns information of
     * possible weights for user.
     *
     * @return custom information object
     */
    public WeightTarget getWeightTarget(){
        return mWeightTarget;
    }

    /**
     * Method sets user default weight.
     *
     * @param weight user current weight.
     */
    public void setDefaultWeight(int weight){
        mDefaultWeight = weight;
    }

    /**
     * Method returns user current weight.
     *
     * @return user current weight.
     */
    public int getDefaultWeight(){
        return mDefaultWeight;
    }
}
