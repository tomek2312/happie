package pl.happie.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import pl.happie.R;
import pl.happie.RoboEventToolbarActivity;
import pl.happie.fragments.LoginFragment;
import roboguice.inject.ContentView;

/**
 * Activity allows user login to application
 * by Facebook or Email.
 *
 * @author Tomasz Trybała
 */
@ContentView(R.layout.activity_login)
public class LoginActivity extends RoboEventToolbarActivity {
    private ProgressDialog mDialog;

    /**
     * Method sets progress dialog, which is shown
     * when data from server is loading.
     */
    private void setProgressDialog() {
        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Loading...");
        mDialog.setCancelable(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setProgressDialog();
        setFragment(new LoginFragment());
    }

    /**
     * Method shows progress dialog.
     */
    public void showDialog() {
        mDialog.show();
    }

    /**
     * Method hides progress dialog.
     */
    public void hideDialog() {
        if (mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    /**
     * Method shows toast.
     *
     * @param message message to user.
     */
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
