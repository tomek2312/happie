package pl.happie.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import pl.happie.R;
import pl.happie.RoboEventToolbarActivity;
import pl.happie.fragments.HelpCrystalsFragment;
import pl.happie.fragments.HelpMealsExercisesFragment;
import pl.happie.views.SlidingTabLayout;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Activity which contains information about
 * meals, exercises and crystals
 *@author Tomasz Trybała
 */
@ContentView(R.layout.activity_help)
public class HelpActivity extends RoboEventToolbarActivity{

    @InjectView(R.id.view_pager)
    protected ViewPager mViewPager;

    @InjectView(R.id.sliding_tabs)
    private SlidingTabLayout mSlidingTabLayout;

    protected HelpMealsExercisesFragment mMealExercisesFragment;
    protected HelpCrystalsFragment mCrystalsFragment;

    /**
     * Method sets adapter to view pager.
     *
     */
    private void setAdapters() {
        PagerAdapter mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);

        mSlidingTabLayout.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.main_green);
            }
        });
        mSlidingTabLayout.setBackgroundColor(getResources().getColor(android.R.color.white));
    }

    /**
     * Method creates new instances of fragments.
     *
     */
    private void setFragments() {
        mMealExercisesFragment = new HelpMealsExercisesFragment();
        mCrystalsFragment = new HelpCrystalsFragment();
    }

    /**
     * Method returns tab name according to
     * current view pager position.
     *
     * @param position current position
     * @return current tab name
     */
    private String getTabName(int position) {
        String name = "";
        switch (position) {
            case 0:
                name = getString(R.string.help_tab_1);
                break;
            case 1:
                name = getString(R.string.help_tab_2);
                break;
        }
        return name;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragments();
        setAdapters();
    }


    private class PagerAdapter extends FragmentPagerAdapter {
        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment item;
            switch (position) {
                case 0:
                    item = mMealExercisesFragment;
                    break;
                case 1:
                    item = mCrystalsFragment;
                    break;
                default:
                    item = new Fragment();
                    break;
            }

            return item;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getTabName(position);
        }
    }
}
