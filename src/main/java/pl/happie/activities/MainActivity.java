package pl.happie.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.eventbus.Subscribe;

import pl.happie.HappieApplication;
import pl.happie.R;
import pl.happie.RoboEventToolbarActivity;
import pl.happie.dialogs.MenuChangePlanDialog;
import pl.happie.dialogs.MenuOptionsDialog;
import pl.happie.dialogs.events.HelpEvent;
import pl.happie.events.AskMealEvent;
import pl.happie.events.OwnMealMenuEvent;
import pl.happie.fonts.RobotoLight;
import pl.happie.fragments.DashboardFragment;
import pl.happie.listeners.DailyPlanListener;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Activity with main content of application.
 *
 * @author Tomasz Trybała
 */
@ContentView(R.layout.activity_main)
public class MainActivity extends RoboEventToolbarActivity {
    @InjectView(R.id.drawer_layout)
    private DrawerLayout mDrawerLayout;

    @InjectView(R.id.txtvDrawerMeal)
    private TextView mTxtvDrawerMeal;

    @InjectView(R.id.txtvDrawerPlan)
    private TextView mTxtvDrawerPlan;

    @InjectView(R.id.txtvDrawerProfile)
    private TextView mTxtvDrawerProfile;

    @InjectView(R.id.txtvDrawerCalendar)
    private TextView mTxtvDrawerCalendar;

    @InjectView(R.id.txtvDrawerSettings)
    private TextView mTxtvDrawerSettings;

    @InjectView(R.id.txtvDrawerFeedback)
    private TextView mTxtvDrawerFeedback;

    @InjectView(R.id.txtvDrawerDiet)
    private TextView mTxtvDrawerDietPremium;

    @InjectView(R.id.llDrawerDiet)
    private LinearLayout mLlDrawerDietNoPremium;

    @InjectView(R.id.llDrawerPremium)
    private LinearLayout mLlDrawerPremium;

    @InjectView(R.id.imgvDrawerTarget1)
    private ImageView mImgvTargetDiet;

    @InjectView(R.id.imgvDrawerTarget2)
    private ImageView mImgvTargetActive;

    @InjectView(R.id.imgvDrawerTarget3)
    private ImageView mImgvTargetRegular;

    private ActionBarDrawerToggle mDrawerToggle;
    private boolean mIsPremium;
    private ImageView mImgvOwnMeal;
    private View mOwnMealView;

    @SuppressLint("InflateParams")
    private void findViews() {
        LayoutInflater inflater = LayoutInflater.from(this);
        mOwnMealView = inflater.inflate(R.layout.view_own_meal, null, false);
        mImgvOwnMeal = (ImageView) mOwnMealView.findViewById(R.id.imgvOwnMealThick);

        mImgvOwnMeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mImgvOwnMeal.isSelected()) {
                    HappieApplication.getEventBus().post(new AskMealEvent());
                }
            }
        });
    }

    /**
     * Method sets custom typeface to text views.
     */
    private void setFonts() {
        mTxtvDrawerMeal.setTypeface(RobotoLight.getTypeface());
    }


    /**
     * Method sets listeners to clickable options in
     * drawer menu.
     */
    private void setDrawerActions() {
        mTxtvDrawerPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo plan fragment
            }
        });

        mTxtvDrawerProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo profile fragment
            }
        });

        mTxtvDrawerCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo calendar fragment
            }
        });

        mTxtvDrawerSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo settings fragment

            }
        });

        mTxtvDrawerFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo feedback fragment
            }
        });

        mTxtvDrawerDietPremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo happie diet fragment
                mIsPremium = !mIsPremium;
                setDrawerAccordingToPremium();
            }
        });

        mLlDrawerPremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo join to premium happie
                mIsPremium = !mIsPremium;
                setDrawerAccordingToPremium();
            }
        });
    }

    /**
     * Method sets content of drawer according to
     * user access to application.
     */
    private void setDrawerAccordingToPremium() {
        if (mIsPremium) {
            mTxtvDrawerDietPremium.setVisibility(View.VISIBLE);
            mLlDrawerDietNoPremium.setVisibility(View.GONE);
        } else {
            mTxtvDrawerDietPremium.setVisibility(View.GONE);
            mLlDrawerDietNoPremium.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Method sets images in drawer according to
     * user progress.
     */
    private void setDrawerTargetImages() {
        mImgvTargetDiet.setImageResource(R.drawable.summary_big_diet_50);
        mImgvTargetActive.setImageResource(R.drawable.summary_big_active_75);
        mImgvTargetRegular.setImageResource(R.drawable.summary_big_regular_100);
    }

    /**
     * Method sets drawer toggle.
     */
    private void setDrawerToggle() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * Method shows menu option dialog.
     */
    private void showMenuOptionsDialog() {
        MenuOptionsDialog dialog = MenuOptionsDialog.getDialog(true, new DailyPlanListener() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onChangeImpossible() {
                MenuChangePlanDialog planDialog = MenuChangePlanDialog.getDialog(false);
                planDialog.show(getSupportFragmentManager(), "dialog menu");
            }

            @Override
            public void onPlanAdapted() {
                MenuChangePlanDialog planDialog = MenuChangePlanDialog.getDialog(true);
                planDialog.show(getSupportFragmentManager(), "dialog menu");
            }
        });

        dialog.show(getSupportFragmentManager(), "dialog menu");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        findViews();
        setFonts();
        setDrawerToggle();
        setDrawerActions();
        setDrawerTargetImages();
        setFragment(new DashboardFragment());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mIsOwnMealFragment) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_main, menu);

            mDrawerToggle.setDrawerIndicatorEnabled(true);
            mImgvOwnMeal.setSelected(false);
        } else {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_own_meal, menu);

            MenuItem share = menu.findItem(R.id.action_own_meal);
            share.setActionView(mOwnMealView);

            mDrawerToggle.setDrawerIndicatorEnabled(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_more) {
            showMenuOptionsDialog();
            return true;
        }

        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Subscribe
    public void goToHelpActivity(final HelpEvent event) {
        Intent intent = new Intent(MainActivity.this, HelpActivity.class);
        startActivity(intent);
    }

    @Subscribe
    public void setOwnMealThick(final OwnMealMenuEvent event) {
        if (mImgvOwnMeal.isSelected()) {
            mImgvOwnMeal.setSelected(false);
        } else {
            mImgvOwnMeal.setSelected(true);
        }
    }
}
