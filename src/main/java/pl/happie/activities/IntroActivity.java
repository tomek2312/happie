package pl.happie.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.common.base.Optional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pl.happie.R;
import pl.happie.RoboEventCompatActivity;
import pl.happie.fonts.RobotoLight;
import pl.happie.fragments.IntroFragment;
import pl.happie.net.push.GcmCredentialsManager;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Activity that allows user to register or login.
 *
 * @author Tomasz Trybała
 */
@ContentView(R.layout.activity_intro)
public class IntroActivity extends RoboEventCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 0;
    private boolean mIntentInProgress;
    private GoogleApiClient mGoogleApiClient;


    @Override
    public void onConnected(Bundle bundle) {
        Log.e("dupa", "onConnected");
        getToken();
    }

    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
//        if (!mIntentInProgress && result.hasResolution()) {
//            try {
//                mIntentInProgress = true;
//                startIntentSenderForResult(result.getResolution().getIntentSender(),
//                        RC_SIGN_IN, null, 0, 0, 0);
//            } catch (IntentSender.SendIntentException e) {
//                The intent was canceled before it was sent.  Return to the default
//                state and attempt to connect to get an updated ConnectionResult.
//                mIntentInProgress = false;
//                mGoogleApiClient.connect();
//            }
//        }
    }

    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }
    }


    private String getToken() {
        try {
            return GoogleAuthUtil.getToken(
                    getApplicationContext(),
                    Plus.AccountApi.getAccountName(mGoogleApiClient), "oauth2:"
                            + Scopes.PLUS_LOGIN + " "
                            + Scopes.PROFILE + " https://www.googleapis.com/auth/plus.profile.emails.read");
        } catch (IOException | GoogleAuthException e) {
            e.printStackTrace();
            return "fail";
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /*package*/ class FragmentsAdapter extends FragmentStatePagerAdapter {
        public FragmentsAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }
    }

    @InjectView(R.id.rlIntroSplash)
    private RelativeLayout mRlSplash;

    @InjectView(R.id.rlIntroContent)
    private RelativeLayout mRlContent;

    @InjectView(R.id.pagerIntro)
    private ViewPager mPager;

    @InjectView(R.id.viewIndicator)
    private View mViewIndicator;

    @InjectView(R.id.llIntroRegister)
    private LinearLayout mLlRegister;

    @InjectView(R.id.llIntroLogin)
    private LinearLayout mLlLogin;

    @InjectView(R.id.txtvIntroGotAccount)
    private TextView mTxtvGotAccount;

    private final List<Fragment> mFragments = new ArrayList<>();

    /**
     * Method adds fragments to list and
     * initializes pager.
     */
    private void initializePager() {
        Resources resources = getResources();
        mFragments.add(IntroFragment.createInstance(R.drawable.ic_splash_3_rules,
                resources.getString(R.string.intro_title_1),
                resources.getString(R.string.intro_content_1),
                resources.getColor(R.color.main_green)));
        mFragments.add(IntroFragment.createInstance(R.drawable.ic_splash_regular,
                resources.getString(R.string.intro_title_2),
                resources.getString(R.string.intro_content_2),
                resources.getColor(R.color.intro_title_orange)));
        mFragments.add(IntroFragment.createInstance(R.drawable.ic_splash_diet,
                resources.getString(R.string.intro_title_3),
                resources.getString(R.string.intro_content_3),
                resources.getColor(R.color.intro_title_pink)));
        mFragments.add(IntroFragment.createInstance(R.drawable.ic_splash_active,
                resources.getString(R.string.intro_title_4),
                resources.getString(R.string.intro_content_4),
                resources.getColor(R.color.intro_title_blue)));


        Resources res = getResources();
        final float padding = 2 * res.getDimensionPixelSize(R.dimen.padding_small);

        mPager.setAdapter(new FragmentsAdapter(getSupportFragmentManager()));
        mPager.setOnPageChangeListener(
                new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        final float indicatorWidth = mViewIndicator.getWidth();
                        mViewIndicator.setTranslationX((position + positionOffset) * (indicatorWidth + padding));
                    }

                    @Override
                    public void onPageSelected(int position) {
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                }
        );
    }

    /**
     * Method sets custom font to views.
     */
    private void setFonts() {
        mTxtvGotAccount.setTypeface(RobotoLight.getTypeface());
    }

    /**
     * Method set listeners to clickable views.
     */
    private void setListeners() {
        mLlLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        mLlRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IntroActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * Method waits for 2 seconds, animates logo
     * and shows info for user.
     */
    private void animateSplash() {
        final ObjectAnimator fadeOut = ObjectAnimator.ofFloat(mRlSplash,
                "alpha", 1.0f, 0.0f);
        fadeOut.setDuration(1000);
        fadeOut.setInterpolator(new AccelerateDecelerateInterpolator());
        fadeOut.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mRlContent.setVisibility(View.VISIBLE);
                mRlSplash.setVisibility(View.GONE);
            }
        });


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                fadeOut.start();
            }
        }, 2000);
    }


    private void registerInGcm() {
        GcmCredentialsManager gcmCredentialsManager = GcmCredentialsManager.getInstance();
        if (gcmCredentialsManager.playServicesAvailable()) {
            Optional<String> optRegistrationId = gcmCredentialsManager.getRegistrationId();
            if (optRegistrationId.isPresent()) {
                Toast.makeText(this, "gcm: " + optRegistrationId.get(), Toast.LENGTH_SHORT).show();
            } else {
                gcmCredentialsManager.registerInBackground(
                        new GcmCredentialsManager.OnGcmRegistrationFinishedListener() {
                            @Override
                            public void onGcmRegistrationFinished(boolean success, @NonNull final Optional<String> optRegistrationId) {
                                if (success) {
                                    System.out.println("gcm: " + optRegistrationId.get());
                                    Toast.makeText(IntroActivity.this, "gcm: "
                                            + optRegistrationId.get(), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(IntroActivity.this, "cannot register in gcm service", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                );
            }
        } else {
            Toast.makeText(IntroActivity.this, "Google Play Services unavailable", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializePager();
        setFonts();
        setListeners();
        animateSplash();
        registerInGcm();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();


    }
}
