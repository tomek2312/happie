
package pl.happie;

/**
 * @author Tomasz Trybała
 * @since 03.03.2015
 */
public class BundleConstants {
    /* Project ID from Google API . */
    public static final String GOOGLE_CLIENT_ID = "211820638107";
    public static final String MIXPANEL_ID = "d186f7583cc95b24cf412edb7b4ef76c";
    public static final String SDF_SIMPLE = "yyyy-MM-dd";
    public static final String FULL_BIRTH_DATE = "dd' 'MMMM' 'yyyy";
    public static final String FULL_DATE_FORMAT = "yyyy-MM-dd' 'HH:mm:ss";
    public static final String FULL_DATE_FORMAT_EXTENDED = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String FULL_DATE_FORMAT_SIMPLE = "yyyy-MM-dd' 'HH:mm";
    public static final String SDF_HOUR = "HH:mm";
    public static final int MAX_PLAN_DAYS = 7;

    public static final int BMI_MIN = 16;
    public static final int BMI_MAX = 40;

    public static final boolean TESTS = true;

    public static final String COOKLET_URL = "http://cooklet.com/";

    public static final String EMPTY = "empty";
    public static final int WEIGHT_ALLOWED_MORE = 10;

    public static final String SMARTWATCH_NOTIFICATION = "pl.happie.smartwatch.NOTIFICATION";
    public static final String SMARTWATCH_NOTIFICATION_TITLE = "message_title";
    public static final String SMARTWATCH_NOTIFICATION_CONTENT = "message_content";
    public static final String SMARTWATCH_NOTIFICATION_MEAL_NUMBER = "message_meal_number";

    public static final String LIVECHAT_ID = "4656341";
    public static final String LIVECHAT_GROUP_ID = "1";
    public static final String LIVECHAT_GROUP_ID_EN = "2";

    public static final String SKU_MONTH = "month";
    public static final String SKU_WEEK = "week";

    public static final String SETTINGS_NAME = "appSettings";
    public static final String PREF_ACCESS_LOGIN_TOKEN = "logged_access_token";
    public static final String PREF_BILLING_AVAILABLE = "pref_billing_available";
    public static final String PREF_FIRST_START = "pref_first_start";
    public static final String PREF_IS_PAID = "pref_is_paid";
    public static final String PREF_CURRENT_PLAN_PAGE = "pref_current_plan_page";
    public static final String PREF_CHANGE_DAYS = "pref_change_days";
    public static final String PREF_CHANGE_OLD_MEAL_ID = "pref_change_old_meal_id";
    public static final String PREF_CHANGE_OLD_MEAL_DAY = "pref_change_old_meal_day";
    public static final String PREF_CHANGE_OLD_MEAL_WEEK_NUMBER = "pref_change_old_meal_week_number";
    public static final String PREF_CHANGE_OLD_MEAL_NUMBER = "pref_change_old_meal_number";
    public static final String PREF_CHANGE_OLD_MEAL_WEEK_ID = "pref_change_old_meal_week_id";
    public static final String PREF_LOGGED_IN = "pref_logged_in";
    public static final String PREF_PROFILE_BASIC = "pref_profile_basic";
    public static final String PREF_FORCE_UPDATE = "pref_force_update";
    public static final String PREF_NOTIFICATION_SETTINGS = "pref_notification_settings";
    public static final String PREF_STARTING_DATE = "pref_starting_date";
    public static final String PREF_IS_MALE = "pref_is_male";
    public static final String PREF_DAYS_LEFT = "pref_days_left";
    public static final String PREF_LANGUAGE = "pref_language";
    public static final String PREF_SHOPPING_LIST_READY = "pref_shopping_list_ready";



}
