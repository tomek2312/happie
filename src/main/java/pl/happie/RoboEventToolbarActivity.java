package pl.happie;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;

import pl.happie.fragments.OwnMealFragment;
import roboguice.inject.InjectView;

/**
 * Activity that extends CompatActivity.
 * Sets automatically Toolbar.
 *
 * @author Tomasz Trybała
 */
public class RoboEventToolbarActivity extends RoboEventCompatActivity {
    @InjectView(R.id.flContainer)
    private FrameLayout mFlContainer;

    protected boolean mIsOwnMealFragment;

    public void setActionBarTitle(String title) {
        setTitle(title);
    }

    @SuppressLint("InflateParams")
    private void prepareLayout() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prepareLayout();
    }


    /**
     * Method hides keyboard if is visible.
     */
    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);

        View view = getCurrentFocus();
        if (view != null) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void setFragment(Fragment fragment) {
        if(fragment instanceof OwnMealFragment){
            mIsOwnMealFragment = true;
        }

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(mFlContainer.getId(), fragment);
        ft.addToBackStack(fragment.getClass().getName());
        ft.commit();
        invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id == android.R.id.home) {
            popBackStack();
            return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        popBackStack();
    }

    private void popBackStack() {
        mIsOwnMealFragment = false;
        hideKeyboard();

        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStack();
            invalidateOptionsMenu();
        } else {
            finish();
        }
    }

    public void clearBackStackToFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(fragment.getClass().getName(), 0);
    }
}