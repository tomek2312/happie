package pl.happie.tools;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Class returns json object
 * with weight parameters.
 *
 * @author Tomasz Trybała
 */
public class WeightUtils {
    private static final String METRIC = "kg";
    private static final String IMPERIAL = "lbs";

    public static JSONObject toJsonObject(int value, boolean isMetric) throws JSONException {
        JSONObject obj = new JSONObject();
        if (isMetric) {
            obj.put(METRIC, value);
        } else {
            obj.put(IMPERIAL, value);
        }
        return obj;
    }
}
