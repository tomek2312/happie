package pl.happie.tools;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Class returns json object
 * with height parameters.
 *
 * @author Tomasz Trybała
 */
public class HeightUtils {
    private static final String METRIC = "cm";
    private static final String IMPERIAL = "ft";
    private static final String IMPERIAL_SECONDARY = "in";

    public static JSONObject toJsonObject(int value, boolean isMetric) throws JSONException {
        JSONObject obj = new JSONObject();
        if (isMetric) {
            obj.put(METRIC, value);
        } else {
            int ft = (value / 24);
            obj.put(IMPERIAL, ft);
            float in = value % 24 == 0 ? 0.0f
                    : (((value % 24 % 2) == 0) ? ((value % 24) / 2)
                    : ((value % 24) / 2) + 0.5f);
            obj.put(IMPERIAL_SECONDARY, in);
        }

        return obj;
    }
}
