package pl.happie.tools;

import android.text.TextUtils;

public class ValidationUtils {
	public final static boolean isValidEmail(CharSequence target) {
		return !TextUtils.isEmpty(target)
				&& android.util.Patterns.EMAIL_ADDRESS.matcher(target)
						.matches();
	}

	public final static boolean isValidPassword(String value) {
		if (value.length() < 6) {
			return false;
		}
		
		/* Must contain at least one digit. */
		return value.matches(".*\\d.*");
	}
}

