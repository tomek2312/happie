
package pl.happie;

public class ConstantsAPI {
    public static final String MAIN_URL = "http://api.dev.happie.pl/api/";
    public static final String UPDATE_PUSH_TOKEN = MAIN_URL + "user/update_push_token";
    public static final String DAY_PLAN_URL = MAIN_URL + "me/week";
    public static final String LIKES_URL = MAIN_URL + "likes";
    public static final String CRISIS_LIKE_URL = MAIN_URL + "crisis_meals/like";
    public static final String LIKES_INGREDIENT_URL = MAIN_URL + "likes/ingredients";
    public static final String LIKES_ACTIVITY_URL = MAIN_URL + "likes/activities";
    public static final String MEAL_SUBSTITUTES = MAIN_URL + "substitutes";
    public static final String MEAL_CRISIS_SUBSTITUTES_PATTERN = MAIN_URL
            + "crisis_meals/%1$s/swap";
    public static final String WORKOUT_SUBSTITUTES = MAIN_URL
            + "workouts/{workout_id}/swap?free={free}&cardio={cardio}&strength={strength}&gym={gym}&home={home}&outdoor={outdoor}";
    public static final String WORKOUT_SUBSTITUTES_POST = MAIN_URL + "swap_activity";

    public static final String MEAL_SWAP = MAIN_URL + "meals/copy";
    public static final String MEAL_CRISIS_SWAP = MAIN_URL + "crisis_meals/copy";
    public static final String SHOPPING_LIST = MAIN_URL + "shopping_list_v2";
    public static final String SHOPPING_LIST_CONSUME = MAIN_URL + "shopping_list_v2/%1$s/%2$s";

    public static final String ME_FORMAT = MAIN_URL + "me?compare_with=%s";
    public static final String ME_BASIC = MAIN_URL + "me/basic";
    public static final String ME_SPECIAL_DIET = MAIN_URL + "special_diet";
    public static final String ME_RECOVER_PASSWORD = MAIN_URL + "user/recovery_password/send_email";
    public static final String ME_WAKE_UP_TIMES = MAIN_URL + "me/wake_up_time";
    public static final String DELETE_MEASUREMENT = MAIN_URL + "user/measurement/%1$s?type=%2$s";
    public static final String WIZARD_BASIC_INFORMATION_URL = MAIN_URL + "user";
    public static final String WIZARD_MEASUREMENTS_URL = MAIN_URL + "survey1";
    public static final String WIZARD_GOALS_URL = MAIN_URL + "survey2";
    public static final String WIZARD_LOGIN_FACEBOOK = MAIN_URL + "user/facebook";
    public static final String WIZARD_LOGIN_GOOGLE = MAIN_URL + "user/google";
    public static final String WIZARD_REGISTER_PASSWORD = MAIN_URL + "user/password";
    public static final String WIZARD_LOGIN_PASSWORD = MAIN_URL + "token/password";
    public static final String BASIC_DATA_URL = MAIN_URL + "me/basic_data";
    public static final String TARGET_WEIGHT_URL = MAIN_URL + "me/target_weight";
    public static final String CURRENT_WEIGHT_URL = MAIN_URL + "me/current_weight";
    public static final String SURVEY_CHECKUP_URL = MAIN_URL + "survey/checkup";

    public static final String FEEDBACK = MAIN_URL + "feedback";
    public static final String CHANGE_PASSWORD = MAIN_URL + "user/change_password";
    public static final String PURCHASE = MAIN_URL + "subscription";
    public static final String SUBSCRIPTION_INFO = MAIN_URL + "subscription_info";
    public static final String MEASUREMENTS = MAIN_URL + "user/measurements?type=";
    public static final String CHANGE_LANGUAGE = MAIN_URL + "change_device_language";
    public static final String WEIGHT_RECOMMENDATIONS = MAIN_URL + "me/get_weight_recommendations";

    public static final String LIKED_WORKOUT_PATTERN = MAIN_URL + "likes/activities/%1$s";
    public static final String LIKED_MEAL_PATTERN = MAIN_URL + "likes/meals/%1$s";
    public static final String LIKED_INGREDIENT_PATTERN = MAIN_URL + "likes/ingredients/%1$s";

    public static final String SCORE = MAIN_URL + "score?premium=%1$s";
    public static final String TICK_EAT = MAIN_URL + "score/meals/ticks";
    public static final String TICK_WORKOUT = MAIN_URL + "score/workouts/ticks";

    public static final String DIET_TYPE_LIST_LOGGED = MAIN_URL + "weeks/currently_supported_diets";
    public static final String DIET_TYPE_LIST = MAIN_URL
            + "{language}/weeks/currently_supported_diets";

    public static final String HEADER_KEY = "X-Access-Token";
    public static final String TEMPORARY_HEADER_KEY = "X-Temporary-Token";
    public static final String RECIPES_URL = "http://cooklet.com/recipe/";

    public static final String REGISTRATION_STEP_1 = MAIN_URL + "registration/1";
    public static final String REGISTRATION_STEP_2 = MAIN_URL + "registration/2";
    public static final String REGISTRATION_STEP_3 = MAIN_URL + "registration/3";
    public static final String REGISTRATION_STEP_4 = MAIN_URL + "registration/4";
    public static final String REGISTRATION_BY_EMAIL = MAIN_URL + "registration/5/password";
    public static final String REGISTRATION_DIET_TYPES = MAIN_URL + "%s/weeks/currently_supported_diets";
}
