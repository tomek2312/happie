package pl.happie.net.push;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.google.common.base.Optional;

import java.util.Random;

/**
 * @author Marcin Przepiórkowski
 * @since 12.01.2015
 */
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
//        StringBuilder content = new StringBuilder();
//        for (String s : intent.getExtras().keySet()) {
//            content.append(s);
//            content.append(": ");
//            content.append(String.valueOf(intent.getExtras().get(s)));
//            content.append("\n");
//        }
//
//        Log.i("IHEALTH_GCM", "RECEIVE: " + content);
//
//        NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(context)
//                        .setSmallIcon(R.drawable.ic_action_send)
//                        .setContentTitle("iHealth push notification")
//                        .setContentText(content);
//        NotificationManager mNotificationManager =
//                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//
//        Random rand = new Random(System.currentTimeMillis());
//        mNotificationManager.notify(rand.nextInt(), mBuilder.build());
//
//        Intent sendIntent = new Intent(IHealthApplication.INTENT_FILTER_PUSH_NOTIFICATION);
//        sendIntent.putExtra(NotificationConstants.EXTRA_NOTIFICATION_TYPE,
//                NotificationUtils.extractPushNotificationType(intent));
//        sendIntent.putExtra(NotificationConstants.EXTRA_NOTIFICATION,
//                NotificationUtils.extractPushNotification(intent));
//
//        Optional<PushNotification> notification = NotificationUtils.extractPushNotification(intent);
//        if (notification.isPresent()) {
//            PushNotificationsCache.getInstance().add(notification.get());
//            LocalBroadcastManager.getInstance(context).sendBroadcast(sendIntent);
//        }
    }
}