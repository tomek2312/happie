package pl.happie.net.push;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.common.base.Optional;

import java.io.IOException;

import pl.happie.HappieApplication;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybała
 */
public class GcmCredentialsManager {
    public interface OnGcmRegistrationFinishedListener {
        void onGcmRegistrationFinished(boolean success, @NonNull Optional<String> optRegistrationId);
    }

    private static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "app_version";
    private static final String SENDER_ID = "211820638107";

    private GoogleCloudMessaging mGcm;
    private static GcmCredentialsManager sManager;

    private static int getAppVersion(@NonNull Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private GcmCredentialsManager() {
    }

    /**
     * Returns valid instance of GcmCredentialsManager.
     *
     * @return  the instance of GcmCredentialsManager
     */
    public static @NonNull GcmCredentialsManager getInstance() {
        if (sManager == null) {
            sManager = new GcmCredentialsManager();
        }

        return sManager;
    }

    /**
     * Checks whether google play services are available on the device.
     *
     * @return true if they are available, false otherwise
     */
    public boolean playServicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(HappieApplication.getAppContext());
        return resultCode == ConnectionResult.SUCCESS;
    }

    /**
     * Registers device in gcm service and asynchronously returns obtained registration. This method
     * should be invoked if the device didn't register in gcm previously or if the stored gcm
     * registration id is not valid (it can be checked with {@link #getRegistrationId} method).
     *
     * @param registrationListener the registration listener
     */
    public void registerInBackground(@NonNull final OnGcmRegistrationFinishedListener registrationListener) {
        checkNotNull(registrationListener, "registrationListener cannot be null");

        AsyncTask<Void, Void, Optional<String>> registrationTask = new AsyncTask<Void, Void, Optional<String>>() {
            @Override
            protected @NonNull Optional<String> doInBackground(Void... params) {
                String registrationId;

                try {
                    if (mGcm == null) {
                        mGcm = GoogleCloudMessaging.getInstance(HappieApplication.getAppContext());
                    }

                    registrationId = mGcm.register(SENDER_ID);
                    HappieApplication.saveToSettings(PROPERTY_REG_ID, registrationId);
                    HappieApplication.saveToSettings(PROPERTY_APP_VERSION, String.valueOf(
                            getAppVersion(HappieApplication.getAppContext())));
                } catch (IOException ex) {
                    registrationId = null;
                }

                return Optional.fromNullable(registrationId);
            }

            @Override
            protected void onPostExecute(Optional<String> optRegistrationId) {
                registrationListener.onGcmRegistrationFinished(optRegistrationId.isPresent(),
                        optRegistrationId);
            }
        };
        registrationTask.execute();
    }

    /**
     * Returns gcm registration id if it was previously stored and it's still valid. If returned
     * id is not valid, then an empty Optional is returned. If this happens, gcm registration process
     * should be invoked with {@link #registerInBackground} method.
     *
     * @return an optional with gcm registration id
     */
    public @NonNull Optional<String> getRegistrationId() {
        String optRegId = HappieApplication.loadStringFromSettings(PROPERTY_REG_ID);
        if (optRegId.isEmpty()) {
            return Optional.fromNullable(null);
        }

        String optAppVersion = HappieApplication.loadStringFromSettings(PROPERTY_APP_VERSION);
        if (optAppVersion.isEmpty()) {
            return Optional.fromNullable(null);
        }

        try {
            int registeredVersion = HappieApplication.loadIntegerFromSettings(PROPERTY_APP_VERSION);
            int currentVersion = getAppVersion(HappieApplication.getAppContext());
            if (registeredVersion != currentVersion) {
                return Optional.fromNullable(null);
            }

            return Optional.fromNullable(optRegId);
        } catch (NumberFormatException exc) {
            return Optional.fromNullable(null);
        }
    }
}