package pl.happie.net;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.PlusClient;
import com.google.gson.JsonObject;

import java.util.Arrays;
import java.util.List;

import pl.happie.ConstantsAPI;
import pl.happie.R;

/**
 * Created by Tomek on 2015-03-18.
 */
public class PostLoginHelper{

    private static final List<String> FACEBOOK_PERMISSIONS = Arrays.asList("email");
    public static final int RC_SIGN_IN = 111;
    public static final String GOOGLE_PLUS_SCOPES = Scopes.PLUS_LOGIN;

    private static final ConnectionResult CONNECTION_RESULT_SUCCESS = new ConnectionResult(
            ConnectionResult.SUCCESS, null);

    protected ProgressDialog progressDialog;

    public PlusClient mGooglePlusClient;
    private ConnectionResult mGoogleConnectionResult;
    private boolean mSignInClicked;
    private boolean mIntentInProgress;

    private TokenListener mGoogleListener;
    private TokenListener mFacebookListener;
    public boolean mIsFacebook = false;
    private Activity mActivity;

    /* Only to be used for registration via wizard. */
    private String mTemporaryToken;

    public PostLoginHelper(Activity activity, String temporaryToken) {
        mActivity = activity;
        mTemporaryToken = temporaryToken;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            onActivityResultGooglePlus(requestCode, resultCode, data);
        } else if (mIsFacebook) {
            Session.getActiveSession().onActivityResult(mActivity, requestCode, resultCode, data);
        }
    }

    public void onDestroy() {
        if (!mGooglePlusClient.isConnecting() || mGooglePlusClient.isConnected()) {
            mGooglePlusClient.disconnect();
        }
    }

    public void onActivityResultGooglePlus(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            if (resultCode != Activity.RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGooglePlusClient.isConnecting()) {
                mGooglePlusClient.connect();
            }
        }
    }

    private void resolveSignInErrors() {
        if (mGoogleConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mGoogleConnectionResult.startResolutionForResult(mActivity, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGooglePlusClient.connect();
            }
        }
    }

    public void getGooglePlusAccessToken(TokenListener listener) {
        mIsFacebook = false;
        mGoogleListener = listener;
        if (!mGooglePlusClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInErrors();
        }
    }

    private void fbSessionOpened(TokenListener listener) {
        Session currentSession = Session.getActiveSession();
        List<String> permissions = currentSession.getPermissions();

        if (permissions.containsAll(FACEBOOK_PERMISSIONS)) {
            String token = currentSession.getAccessToken();
            listener.onAccessTokenReceived(token);
        } else {
            Session.NewPermissionsRequest reauthRequest = new Session.NewPermissionsRequest(
                    mActivity, FACEBOOK_PERMISSIONS);
            reauthRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
            reauthRequest.setCallback(mFacebookStatusCallback);

            currentSession.requestNewPublishPermissions(reauthRequest);
        }
    }

    public void getFacebookAccessToken(TokenListener listener) {
        mIsFacebook = true;
        mFacebookListener = listener;
        Session currentSession = Session.getActiveSession();

        if (currentSession == null || currentSession.getState().isClosed()) {
            Session session = new Session(mActivity.getApplicationContext());
            Session.setActiveSession(session);
            currentSession = session;
        }

        if (currentSession.isOpened()) {
            fbSessionOpened(listener);
        } else {
            Session.OpenRequest openRequest = new Session.OpenRequest(mActivity);
            openRequest.setPermissions(FACEBOOK_PERMISSIONS);
            openRequest.setCallback(mFacebookStatusCallback);

            currentSession.openForRead(openRequest);
        }
    }

    private final Session.StatusCallback mFacebookStatusCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            if (exception != null) {
                session.closeAndClearTokenInformation();
                Session.setActiveSession(session);
                mFacebookListener.onError(exception);
            } else if (session.isClosed()) {
                session.closeAndClearTokenInformation();
                Session.setActiveSession(null);
                mFacebookListener.onError(null);
            } else if (session.isOpened()) {
                fbSessionOpened(mFacebookListener);
            }
        }
    };

    public interface TokenListener {

        public void onAccessTokenReceived(String accessToken);

        public void onError(Exception exception);

    }


    /**
     * Interprets response from the server. Displays an error message upon
     * unsuccessful login.
     */
    public void onSignUpStatus(int code, JsonObject status) {
        boolean login = false;// code == HttpStatus.SC_CREATED;//TODO

        if (status.has("error")) {
            if (login) {
                Toast.makeText(mActivity, mActivity.getString(R.string.wrong_credentials),
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(mActivity, mActivity.getString(R.string.account_not_created),
                        Toast.LENGTH_LONG).show();
            }
            return;
        }

        if (!status.get("data").getAsJsonObject().has(ConstantsAPI.HEADER_KEY)) {
            Log.e("dupa", "error1");
            return;
        }

        String token = status.get("data").getAsJsonObject().get(ConstantsAPI.HEADER_KEY)
                .getAsString();
        if (!login) {
            Intent data = new Intent();
            data.putExtra("new", true);
            mActivity.setResult(Activity.RESULT_OK, data);
        } else {
            mActivity.setResult(Activity.RESULT_OK);
        }
        mActivity.finish();
    }

    public void onLoginStatus(int code, JsonObject status) {
        boolean login = false;// code == HttpStatus.SC_CREATED;//TODO

        if (status.has("error")) {
            if (login) {
                Toast.makeText(mActivity, mActivity.getString(R.string.wrong_credentials),
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(mActivity, mActivity.getString(R.string.account_not_created),
                        Toast.LENGTH_LONG).show();
            }
            return;
        }

        if (!status.has(ConstantsAPI.HEADER_KEY)) {
            Log.e("dupa", "error2");
            return;
        }

        String token = status.get(ConstantsAPI.HEADER_KEY).getAsString();
        if (!login) {
            Intent data = new Intent();
            data.putExtra("new", true);
            mActivity.setResult(Activity.RESULT_OK, data);
        } else {
            mActivity.setResult(Activity.RESULT_OK);
        }
        mActivity.finish();
    }

    /**
     * Interprets response from the server. Displays an error message upon
     * unsuccessful login.
     */
    public void onLoginStatus(JsonObject status) {
        if (status.has("error")) {
            Toast.makeText(mActivity, mActivity.getString(R.string.login_unsuccessful),
                    Toast.LENGTH_LONG).show();
            return;
        }

        if (!status.get("data").getAsJsonObject().has(ConstantsAPI.HEADER_KEY)) {
            return;
        }

        String token = status.get("data").getAsJsonObject().get(ConstantsAPI.HEADER_KEY)
                .getAsString();
        Log.e("dupa", "token: "+token);
    }

    public void onGooglePlusError(Exception exception) {
        hideProgressDialog();

        Toast.makeText(mActivity, mActivity.getString(R.string.google_login_failed),
                Toast.LENGTH_LONG).show();
    }

    public void onFacebookError(Exception exception) {
        hideProgressDialog();

        Toast.makeText(mActivity, mActivity.getString(R.string.facebook_login_failed),
                Toast.LENGTH_LONG).show();
    }

    public void hideProgressDialog() {
        try {

            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (IllegalArgumentException e) {
            //should not happen, just in case
        }
    }

    public void showProgressDialog(String msg) {
        hideProgressDialog();

        progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage(msg);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
}
