package pl.happie.net.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author Tomasz Trybała
 */
public class WeightTarget {
    @SerializedName("min_weight")
    private WeightItem mMinWeight;

    @SerializedName("max_weight")
    private WeightItem mMaxWeight;

    @SerializedName("weight_in_time_list")
    private WeightTargetEntry[] mEntryList;

    public WeightItem getMinWeight() {
        return mMinWeight;
    }

    public WeightItem getMaxWeight() {
        return mMaxWeight;
    }

    public WeightTargetEntry[] getEntryList() {
        return mEntryList;
    }
}
