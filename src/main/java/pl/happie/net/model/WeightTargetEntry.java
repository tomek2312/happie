package pl.happie.net.model;

import com.google.gson.annotations.SerializedName;

/**
 *@author Tomasz Trybała
 */
public class WeightTargetEntry {
    @SerializedName("weeks")
    private int mWeeks;

    @SerializedName("weight")
    private WeightItem mWeight;

    public int getWeeks(){
        return mWeeks;
    }

    public WeightItem getWeight(){
        return mWeight;
    }
}
