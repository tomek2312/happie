package pl.happie.net.model;

import com.google.gson.annotations.SerializedName;

/**
 *@author Tomasz Trybała
 */
public class DietTypeItem {
    @SerializedName("translation")
    private String mName;

    @SerializedName("diet_code")
    private int mDietCode;

    public String getName(){
        return mName;
    }

    public int getDietCode(){return mDietCode;}
}
