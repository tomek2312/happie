package pl.happie.net.model;

import com.google.gson.annotations.SerializedName;

/**
 *@author Tomasz Trybała
 */
public class ErrorResponse {
    @SerializedName("status_code")
    private int mStatusCode;

    public int getStatusCode(){
        return mStatusCode;
    }
}
