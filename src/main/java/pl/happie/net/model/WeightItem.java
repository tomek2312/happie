package pl.happie.net.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author Tomasz Trybała
 */
public class WeightItem {
    @SerializedName("lbs")
    private int mImperial;

    @SerializedName("kg")
    private int mMetric;

    public int getWeight() {
        return mImperial == 0 ?
                mMetric :
                mImperial;
    }
}
