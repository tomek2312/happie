package pl.happie.net.model;

import com.google.gson.annotations.SerializedName;

/**
 *@author Tomasz Trybała
 */
public class DietType {
    @SerializedName("data")
    private DietTypeItem[] mEntryList;

    public DietTypeItem[] getEntryList(){
        return mEntryList;
    }
}
