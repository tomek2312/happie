package pl.happie.net.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author Tomasz Trybała
 */
public class DayNote {
    @SerializedName("activity_index_percent")
    private float mActivityIndexPercent;

    @SerializedName("recommented_meals_index_percent")
    private float mRecommentedMealsIndexPercent;

    @SerializedName("regularity_index_percent")
    private float mRegularityIndexPercent;

    @SerializedName("note")
    private String mNote;

    public float getActivityPercent() {
        return mActivityIndexPercent;
    }

    public float getDietPercent() {
        return mRecommentedMealsIndexPercent;
    }

    public float getRegularPercent() {
        return mRegularityIndexPercent;
    }

    public DayNoteType getType() {
        if (mNote.equals(DayNoteType.HAPPIE.getName())) {
            return DayNoteType.HAPPIE;
        } else if (mNote.equals(DayNoteType.SADDIE.getName())) {
            return DayNoteType.SADDIE;
        } else {
            return DayNoteType.NEUTRAL;
        }
    }
}
