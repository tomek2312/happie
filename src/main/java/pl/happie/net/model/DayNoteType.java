package pl.happie.net.model;

/**
 * @author Tomasz Trybała
 */
public enum DayNoteType {
    HAPPIE("happie"),
    SADDIE("saddie"),
    NEUTRAL("neutral");

    private String mName;

    private DayNoteType(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }
}
