package pl.happie.net.requests;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *@author Tomasz Trybała
 */
public class RegisterWakeUpRequest {
    public static JSONObject getJson(String weekTime, String weekendTime, Context context){
        JSONObject object = new JSONObject();
        try {
            object.put("wake_up_time_monday", weekTime);
            object.put("wake_up_time_tuesday", weekTime);
            object.put("wake_up_time_wednesday", weekTime);
            object.put("wake_up_time_thursday", weekTime);
            object.put("wake_up_time_friday", weekTime);
            object.put("wake_up_time_saturday", weekendTime);
            object.put("wake_up_time_sunday", weekendTime);
        }catch (JSONException e) {
            Toast.makeText(context, "Error!", Toast.LENGTH_SHORT).show();
        }

        return object;
    }
}
