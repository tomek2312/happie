package pl.happie.net.requests;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import pl.happie.tools.WeightUtils;

/**
 *@author Tomasz Trybała
 */
public class RegisterTargetRequest {
    public static JSONObject getJson(int weight, boolean isMetric, int diet, Context context){
        JSONObject object = new JSONObject();
        try {
            object.put("target_weight", (WeightUtils.toJsonObject(weight, isMetric)));
            object.put("special_diet", diet);
        }catch (JSONException e) {
            Toast.makeText(context, "Error!", Toast.LENGTH_SHORT).show();
        }

        return object;
    }
}
