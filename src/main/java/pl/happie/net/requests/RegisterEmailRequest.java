package pl.happie.net.requests;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *@author Tomasz Trybała
 */
public class RegisterEmailRequest {
    public static JSONObject getJson(String name, String email, String password, Context context){
        JSONObject object = new JSONObject();
        try {
            object.put("email", email);
            object.put("password", password);
            object.put("name", name);
        }catch (JSONException e) {
            Toast.makeText(context, "Error!", Toast.LENGTH_SHORT).show();
        }

        return object;
    }

    public static JSONObject getJson(String email, String password, Context context){
        JSONObject object = new JSONObject();
        try {
            object.put("email", email);
            object.put("password", password);
        }catch (JSONException e) {
            Toast.makeText(context, "Error!", Toast.LENGTH_SHORT).show();
        }

        return object;
    }
}
