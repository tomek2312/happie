package pl.happie.net.requests;

import android.app.Activity;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.google.common.base.Optional;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.TimeZone;

import pl.happie.HappieApplication;
import pl.happie.R;
import pl.happie.net.push.GcmCredentialsManager;
import pl.happie.tools.HeightUtils;
import pl.happie.tools.WeightUtils;

/**
 *@author Tomasz Trybała
 */
public class RegisterInfoRequest{
    public static JSONObject getJson(Activity context, int age, boolean isMale, boolean isMetric, int height, int weight){
        JSONObject object = new JSONObject();
        Optional<String> optPushKey = GcmCredentialsManager.getInstance().getRegistrationId();
        String pushKey = optPushKey.isPresent() ? optPushKey.get() : "no_key";
        try {
            object.put("age", age);
            object.put("sex", isMale);
            object.put("device_identity_hash", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
            object.put("device_push_key", pushKey);
            object.put("device_resolution", HappieApplication.getScreenWidth(context));
            object.put("device_language", Locale.getDefault().getLanguage());
            object.put("timezone", TimeZone.getDefault().getID());
            object.put("device_os", "android");
            object.put("device_model", context.getResources().getString(R.string.screen_type));
            object.put("units", isMetric ? "metric" : "imperial");
            object.put("height", (HeightUtils.toJsonObject(height, isMetric)));
            object.put("current_weight", (WeightUtils.toJsonObject(weight, isMetric)));
        }catch (JSONException e) {
            Toast.makeText(context, "Error!", Toast.LENGTH_SHORT).show();
        }

        return object;
    }
}
