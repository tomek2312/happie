package pl.happie.net.requests;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *@author Tomasz Trybała
 */
public class RegisterFigureRequest {
    public static JSONObject getJson(int wrist, int body, int history, Context context){
        JSONObject object = new JSONObject();
        try {
            object.put("basic_finger_measure", wrist);
            object.put("body_profile", body);
            object.put("child_body_profile", history);
        }catch (JSONException e) {
            Toast.makeText(context, "Error!", Toast.LENGTH_SHORT).show();
        }

        return object;
    }
}
