package pl.happie.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import pl.happie.R;
import pl.happie.fonts.RobotoLight;
import roboguice.fragment.RoboDialogFragment;

/**
 * Dialog with possibility to adapt daily plan.
 *
 * @author Tomasz Trybała
 */
public class EarlyWakeUpDialog extends RoboDialogFragment {
    private LinearLayout mLlAdapt;
    private LinearLayout mLlDefault;
    private AlertDialog mDialog;

    @Override
    public
    @NonNull
    Dialog onCreateDialog(@NonNull Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_main_early_wake_up, null, false);

        mLlAdapt = (LinearLayout) dialogView.findViewById(R.id.llDialogEarlyWakeUpAdapt);
        mLlDefault = (LinearLayout) dialogView.findViewById(R.id.llDialogEarlyWakeUpDefault);
        ((TextView) dialogView.findViewById(R.id.txtvDialogEarlyWakeUpDesc)).
                setTypeface(RobotoLight.getTypeface());

        setListeners();

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setView(dialogView);

        mDialog = dialogBuilder.create();
        mDialog.setCancelable(false);
        mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        return mDialog;
    }


    private void setListeners() {
        mLlAdapt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo adapt plan
            }
        });

        mLlDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
    }
}