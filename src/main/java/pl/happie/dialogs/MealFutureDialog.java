package pl.happie.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import pl.happie.R;
import roboguice.fragment.RoboDialogFragment;

/**
 * Dialog with options to meal that will be eaten by user.
 *
 * @author Tomasz Trybała
 */
public class MealFutureDialog extends RoboDialogFragment {
    private LinearLayout mLlChange;
    private LinearLayout mLlCopy;
    private TextView mTxtvTitle;
    private String mMeal;
    private AlertDialog mDialog;

    /**
     * Method sets meal name.
     *
     * @param meal meal name
     */
    private void setMeal(String meal) {
        mMeal = meal;
    }

    /**
     * Method sets content according to flag if plan is default.
     */
    private void setContent() {
        mTxtvTitle.setText(getResources().getString(R.string.dialog_meal_future_title, mMeal));
    }

    /**
     *Method sets listeners to clickable views.
     */
    private void setListeners() {
        mLlChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo change meal
            }
        });

        mLlCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo copy meal to another day
            }
        });
    }

    /**
     * Method find views.
     *
     * @param root root view
     */
    private void findViews(View root) {
        mLlChange = (LinearLayout) root.findViewById(R.id.llDialogMealFutureChange);
        mLlCopy = (LinearLayout) root.findViewById(R.id.llDialogMealFutureCopy);
        mTxtvTitle = (TextView) root.findViewById(R.id.txtvDialogMealFutureTitle);
    }

    public static MealFutureDialog getDialog(String meal) {
        MealFutureDialog dialog = new MealFutureDialog();
        dialog.setMeal(meal);

        return dialog;
    }

    @Override
    public
    @NonNull
    Dialog onCreateDialog(@NonNull Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_meal_future, null, false);

        findViews(dialogView);
        setListeners();
        setContent();

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setView(dialogView);

        mDialog = dialogBuilder.create();
        mDialog.setCancelable(true);
        mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        return mDialog;
    }
}
