package pl.happie.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import pl.happie.HappieApplication;
import pl.happie.R;
import pl.happie.dialogs.events.HelpEvent;
import pl.happie.listeners.DailyPlanListener;
import roboguice.fragment.RoboDialogFragment;

/**
 * Dialog that shows options in
 * MainActivity.
 *
 *@author Tomasz Trybała
 */
public class MenuOptionsDialog extends RoboDialogFragment{
    private LinearLayout mLlCrisis;
    private LinearLayout mLlSummation;
    private LinearLayout mLlHelp;
    private LinearLayout mLlDefault;
    private TextView mTxtvChangePlan;
    private boolean mIsDefault;
    private AlertDialog mDialog;
    private DailyPlanListener mListener;

    /**
     * Method sets flag if plan is default or not.
     *
     * @param isDefault if plan is default
     */
    private void setIsDefaultPlan(boolean isDefault) {
        mIsDefault = isDefault;
    }

    /**
     * Method sets daily plan listener.
     *
     * @param listener daily plan listener
     */
    private void setDailyPlanListener(DailyPlanListener listener){
        mListener = listener;
    }

    /**
     * Method sets content according to flag if plan is default.
     *
     */
    private void setContent() {
        mTxtvChangePlan.setText(getResources().getString(mIsDefault ?
                R.string.dialog_main_default :
                R.string.dialog_main_not_default));
    }

    /**
     * Method sets listeners to clickable views.
     */
    private void setListeners() {
        mLlCrisis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo crisis meal
            }
        });

        mLlSummation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo summation
            }
        });

        mLlHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HappieApplication.getEventBus().post(new HelpEvent());
                mDialog.dismiss();
            }
        });

        mLlDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                //todo check it
                mListener.onPlanAdapted();
            }
        });
    }

    /**
     * Method find views.
     *
     * @param root root view
     */
    private void findViews(View root){
        mLlCrisis = (LinearLayout) root.findViewById(R.id.llDialogCrisis);
        mLlSummation = (LinearLayout) root.findViewById(R.id.llDialogSummation);
        mLlHelp = (LinearLayout) root.findViewById(R.id.llDialogHelp);
        mLlDefault = (LinearLayout) root.findViewById(R.id.llDialogChangePlan);
        mTxtvChangePlan = (TextView) root.findViewById(R.id.txtvDialogChangePlan);
    }

    public static MenuOptionsDialog getDialog(boolean isDefault, DailyPlanListener listener) {
        MenuOptionsDialog dialog = new MenuOptionsDialog();
        dialog.setIsDefaultPlan(isDefault);
        dialog.setDailyPlanListener(listener);

        return dialog;
    }

    @Override
    public @NonNull Dialog onCreateDialog(@NonNull Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_main_options, null, false);

        findViews(dialogView);
        setListeners();
        setContent();

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setView(dialogView);

        mDialog = dialogBuilder.create();
        mDialog.setCancelable(true);
        mDialog.getWindow().clearFlags( WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        return mDialog;
    }
}
