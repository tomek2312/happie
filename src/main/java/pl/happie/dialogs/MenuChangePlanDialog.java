package pl.happie.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import pl.happie.R;
import pl.happie.fonts.RobotoLight;
import roboguice.fragment.RoboDialogFragment;

/**
 * Dialog that shows information about
 * change of daily plan.
 *
 * @author Tomasz Trybała
 */
public class MenuChangePlanDialog extends RoboDialogFragment {
    private TextView mTxtvOk;
    private TextView mTxtvTitle;
    private TextView mTxtvDescription;
    private AlertDialog mDialog;
    private boolean mIsAdapted;

    /**
     * Method sets flag if plan is already adapted
     * or is no possibility of changing plan.
     *
     * @param isAdapted if plan is adapted
     */
    private void setIsAdapted(boolean isAdapted) {
        mIsAdapted = isAdapted;
    }

    /**
     * Method find all views.
     *
     * @param root view root
     */
    private void findViews(View root){
        mTxtvTitle = (TextView) root.findViewById(R.id.txtvDialogChangePlanTitle);
        mTxtvDescription = (TextView) root.findViewById(R.id.txtvDialogChangePlanDesc);
        mTxtvOk = (TextView) root.findViewById(R.id.txtvDialogChangePlanOk);
    }

    /**
     * Method sets custom typeface to text view.
     */
    private void setFonts(){
        mTxtvDescription.setTypeface(RobotoLight.getTypeface());
    }


    /**
     * Method sets listeners to clickable views.
     */
    private void setListeners() {
        mTxtvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
    }

    /**
     * Method sets content according to flag if plan is adapted.
     *
     */
    private void setContent() {
        mTxtvTitle.setText(getResources().getString(mIsAdapted ?
                R.string.dialog_change_plan_title_adapted :
                R.string.dialog_change_plan_title_impossible));
        mTxtvDescription.setText(getResources().getString(mIsAdapted ?
                R.string.dialog_change_plan_desc_adapted :
                R.string.dialog_change_plan_desc_impossible));
    }

    public static MenuChangePlanDialog getDialog(boolean possibility) {
        MenuChangePlanDialog dialog = new MenuChangePlanDialog();
        dialog.setIsAdapted(possibility);

        return dialog;
    }

    @Override
    public
    @NonNull
    Dialog onCreateDialog(@NonNull Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_main_change_plan, null, false);

        findViews(dialogView);
        setListeners();
        setContent();
        setFonts();

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setView(dialogView);

        mDialog = dialogBuilder.create();
        mDialog.setCancelable(true);
        mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        return mDialog;
    }
}
