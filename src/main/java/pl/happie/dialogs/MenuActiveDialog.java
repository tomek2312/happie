package pl.happie.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import pl.happie.R;
import roboguice.fragment.RoboDialogFragment;

/**
 * Dialog that shows options of daily activity in
 * MainActivity.
 *
 * @author Tomasz Trybała
 */
public class MenuActiveDialog extends RoboDialogFragment {
    private LinearLayout mLlYes;
    private LinearLayout mLlNo;
    private AlertDialog mDialog;

    @Override
    public
    @NonNull
    Dialog onCreateDialog(@NonNull Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_main_active, null, false);

        mLlYes = (LinearLayout) dialogView.findViewById(R.id.llDialogActiveYes);
        mLlNo = (LinearLayout) dialogView.findViewById(R.id.llDialogActiveNo);

        setListeners();

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setView(dialogView);

        mDialog = dialogBuilder.create();
        mDialog.setCancelable(true);
        mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        return mDialog;
    }


    private void setListeners() {
        mLlYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo crisis meal
            }
        });

        mLlNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo summation
            }
        });
    }
}
