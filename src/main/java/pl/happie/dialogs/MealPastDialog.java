package pl.happie.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import pl.happie.R;
import pl.happie.activities.MainActivity;
import pl.happie.fragments.OwnMealFragment;
import pl.happie.listeners.ChangeMealListener;
import roboguice.fragment.RoboDialogFragment;

/**
 * Dialog with options to meal that was eaten by user.
 *
 * @author Tomasz Trybała
 */
public class MealPastDialog extends RoboDialogFragment {
    private LinearLayout mLlRecommended;
    private LinearLayout mLlCrisis;
    private LinearLayout mLlOther;
    private LinearLayout mLlNothing;
    private TextView mTxtvTitle;
    private String mMeal;
    private AlertDialog mDialog;
    private MainActivity mActivity;
    private ChangeMealListener mListener;

    /**
     * Method sets change plan listener.
     *
     * @param listener change plan listener
     */
    private void setChangeMealListener(ChangeMealListener listener) {
        mListener = listener;
    }

    /**
     * Method sets current activity.
     *
     * @param activity main activity
     */
    private void setActivity(MainActivity activity) {
        mActivity = activity;
    }

    /**
     * Method sets meal name.
     *
     * @param meal meal name
     */
    private void setMeal(String meal) {
        mMeal = meal;
    }

    /**
     * Method sets content according to flag if plan is default.
     */
    private void setContent() {
        mTxtvTitle.setText(getResources().getString(R.string.dialog_meal_past_title, mMeal));
    }

    private void setListeners() {
        mLlRecommended.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo eat recommended meal
            }
        });

        mLlCrisis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo eat crysis meal
            }
        });

        mLlOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                OwnMealFragment fragment = OwnMealFragment.getFragment(new ChangeMealListener() {
                    @Override
                    public void getMealName(String mealName) {
                        mListener.getMealName(mealName);
                    }
                });
                mActivity.setFragment(fragment);
            }
        });

        mLlNothing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo eat nothnig
            }
        });
    }

    private void findViews(View root) {
        mLlRecommended = (LinearLayout) root.findViewById(R.id.llDialogMealPastRecommended);
        mLlCrisis = (LinearLayout) root.findViewById(R.id.llDialogMealPastCrisis);
        mLlOther = (LinearLayout) root.findViewById(R.id.llDialogMealPastOther);
        mLlNothing = (LinearLayout) root.findViewById(R.id.llDialogMealPastNothing);
        mTxtvTitle = (TextView) root.findViewById(R.id.txtvDialogMealPastTitle);
    }

    public static MealPastDialog getDialog(MainActivity activity, String meal, ChangeMealListener listener) {
        MealPastDialog dialog = new MealPastDialog();
        dialog.setMeal(meal);
        dialog.setActivity(activity);
        dialog.setChangeMealListener(listener);

        return dialog;
    }

    @Override
    public
    @NonNull
    Dialog onCreateDialog(@NonNull Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_meal_past, null, false);

        findViews(dialogView);
        setListeners();
        setContent();

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setView(dialogView);

        mDialog = dialogBuilder.create();
        mDialog.setCancelable(true);
        mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        return mDialog;
    }
}
